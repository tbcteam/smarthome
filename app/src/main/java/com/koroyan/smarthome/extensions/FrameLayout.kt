package com.koroyan.smarthome.extensions

import android.widget.FrameLayout
import com.koroyan.smarthome.App
import com.koroyan.smarthome.R

var FrameLayout.checked: Boolean
    get() = tag == true
    set(value) {
        check(value)
    }

fun FrameLayout.check(check:Boolean){
    tag = check
    background = if (check)
        App.instance.context?.getDrawable(R.drawable.ic_icons_oval_checked)
    else
        App.instance.context?.getDrawable(R.drawable.ic_icons_oval_unchecked)
}