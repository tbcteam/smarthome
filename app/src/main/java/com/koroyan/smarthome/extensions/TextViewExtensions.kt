package com.koroyan.smarthome.extensions

import android.graphics.Color
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.TextView
import com.captaindroid.tvg.Tvg
import kotlinx.android.synthetic.main.song_recyclerview_layout.view.*

var TextView.isClicked: Boolean
    get() = tag == true
    set(value) {
        click(value)
    }

fun TextView.click(value: Boolean) {
    tag = value
    visibility = if (value)
        VISIBLE
    else
        GONE
}

fun TextView.setGradientColor() {
    Tvg.change(
        this, intArrayOf(
            Color.parseColor("#d73772"),
            Color.parseColor("#e57245"),
            Color.parseColor("#f1a41e")
        )
    )
}