package com.koroyan.smarthome.extensions

import android.widget.ImageView
import com.koroyan.smarthome.R

var ImageView.isClicked: Boolean
    get() = tag == true
    set(value) {
        click(value)
    }

fun ImageView.click(value: Boolean) {
    tag = value
    if (value)
        setImageResource(R.mipmap.baseline_keyboard_arrow_up_white_36)
    else
        setImageResource(R.mipmap.baseline_keyboard_arrow_down_white_36)
}