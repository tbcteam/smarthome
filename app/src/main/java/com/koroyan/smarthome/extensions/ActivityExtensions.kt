package com.koroyan.smarthome.extensions

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import com.koroyan.smarthome.R
import kotlinx.android.synthetic.main.layout_toolbar.*

fun Activity.coosImage(request:Int) {
    val intent = Intent()
    intent.type = "image/*"
    intent.action = Intent.ACTION_GET_CONTENT
    startActivityForResult(Intent.createChooser(intent, "Select Picture"), request)
}

 fun AppCompatActivity.setToolBar(title:String,homeEnabled:Boolean) {
    setSupportActionBar(mToolbar)
    supportActionBar!!.setDisplayShowTitleEnabled(false)
    supportActionBar!!.setDisplayHomeAsUpEnabled(homeEnabled)
    toolBarTitleTextView.text = title
}