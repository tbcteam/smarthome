package com.koroyan.smarthome.utils

object IntentKeys {
    const val ADD_IMAGE_REQUEST = 100
    const val UPDATE_IMAGE_REQUEST_DATABASE = 101
    const val CATEGORY = "category"

    const val CHANGE_ROOM_REQUEST = 1
    const val ROOM = "room:key"
    const val OLD_ROOM_NAME = "old_room_name:key"

    const val LIVE_CAMERA = "live_camera:key"
}