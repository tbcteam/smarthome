package com.koroyan.smarthome.data.preferencedata

import android.content.Context
import com.koroyan.smarthome.App

object SharedPreferences {

    const val ID = "userID"
    const val MAIL_FOR_FIREBASE = "mailForFirebase"
    const val TOKEN = "token"

    const val IS_REMEMBERED = ""

    private val preferences by lazy {
        App.instance.context!!.getSharedPreferences("data", Context.MODE_PRIVATE)
    }
    private val editor by lazy {
        preferences.edit()
    }

    fun saveString(key: String, value: String) {
        editor.putString(key, value)
        editor.commit()
    }

    fun getString(key: String): String? {
        return preferences.getString(key, "")
    }

    fun saveBoolean(key: String, value: Boolean) {
        editor.putBoolean(key, value)
        editor.commit()
    }

    fun getBoolean(key: String): Boolean? {
        return preferences.getBoolean(key, false)
    }

    fun removeString(key: String): Boolean {
        if (!preferences.contains(key))
            return false
        editor.remove(key)
        editor.commit()
        return true
    }

    fun clearAll() {
        editor.clear()
        editor.commit()
    }
}