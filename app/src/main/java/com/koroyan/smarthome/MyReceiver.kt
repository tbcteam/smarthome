package com.koroyan.smarthome

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.widget.Toast
import java.lang.NullPointerException

class MyReceiver : BroadcastReceiver() {

    var lastToast = ""
    override fun onReceive(context: Context, intent: Intent) {

        if (!isOnline(context)) {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show()
            lastToast = "No Internet Connection"
        }

        if (lastToast == "No Internet Connection" && isOnline(context)) {
            Toast.makeText(context, "Back Online", Toast.LENGTH_SHORT).show()
            lastToast = ""
        }

    }

    private fun isOnline(context: Context): Boolean {
        return try {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val netInfo = cm.activeNetworkInfo
            netInfo != null && netInfo.isConnected
        } catch (e: NullPointerException) {
            e.printStackTrace()
            false
        }
    }
}
