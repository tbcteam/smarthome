package com.koroyan.smarthome.services

import android.content.SharedPreferences
import android.util.Log.d
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.koroyan.smarthome.network.firebase.Firebase


class MyFirebaseMessagingService: FirebaseMessagingService() {
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        d("FCMMESSAGING", "From: " + remoteMessage.from)
        if (remoteMessage.data.isNotEmpty()) {
            d("FCMMESSAGING", "Message data payload: " + remoteMessage.data)

        }

        // Check if message contains a notification payload.
        if (remoteMessage.notification != null) {
            d("FCMMESSAGING", "Message Notification Body: " + remoteMessage.notification!!.body)
        }
    }
    override fun onNewToken(token: String) {
        d("FCMMESSAGING", "Refreshed token: $token")
        com.koroyan.smarthome.data.preferencedata.SharedPreferences.saveString(com.koroyan.smarthome.data.preferencedata.SharedPreferences.TOKEN,token)
     // Firebase.setToken(token)
    }
}