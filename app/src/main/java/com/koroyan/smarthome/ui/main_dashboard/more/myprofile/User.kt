package com.koroyan.smarthome.ui.main_dashboard.more.myprofile

class User(
    var name: String = "",
    var email: String = "",
    var phone: String = "",
    var profileImage: String = "empty"
)