package com.koroyan.smarthome.ui.main_dashboard.rooms.addrooms.adapters

import android.view.View

interface AddImageClickListener {
   fun coosImage(v: View)
   fun addImage()
}