package com.koroyan.smarthome.ui.main_dashboard.music.song

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.koroyan.smarthome.R
import com.koroyan.smarthome.extensions.setGradientColor
import com.koroyan.smarthome.extensions.setToolBar
import com.koroyan.smarthome.network.CustomCallback
import com.koroyan.smarthome.network.HttpRequest
import com.koroyan.smarthome.tools.Tools
import kotlinx.android.synthetic.main.activity_song_lyrics.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import org.json.JSONObject

class SongLyricsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_song_lyrics)
        init()
    }

    private fun init() {
        setToolBar(getString(R.string.song), true)

        val artist = intent.getStringExtra("artist")
        val title = intent.getStringExtra("title")

        HttpRequest.getRequest(artist!!, title!!, object: CustomCallback {
            override fun onFailure(error: String) {
                Toast.makeText(this@SongLyricsActivity, error, Toast.LENGTH_LONG).show()
            }

            override fun onResponse(response: String) {
                val lyrics = JSONObject(response).getString("lyrics")
                songTitleTextView.text = title
                songTitleTextView.setGradientColor()
                songLyricsTextView.text = lyrics
            }

        })
        playSongImageButton.visibility = View.VISIBLE
        playSongImageButton.setOnClickListener {
            Tools.errorDialog(this, "Invalid Request", "Could not load the song, no device was attached.")
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            super.onBackPressed()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        }
        return super.onOptionsItemSelected(item)
    }
}