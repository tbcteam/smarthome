package com.koroyan.smarthome.ui.main_dashboard.more.editRooms

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.PopupMenu
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.koroyan.smarthome.R
import com.koroyan.smarthome.callbacks.OnStatusListener
import com.koroyan.smarthome.extensions.setToolBar
import com.koroyan.smarthome.tools.Tools
import com.koroyan.smarthome.ui.main_dashboard.more.editRooms.adapters.category.CategoryAdapter
import com.koroyan.smarthome.ui.main_dashboard.more.editRooms.adapters.category.OnItemClickListener
import com.koroyan.smarthome.ui.main_dashboard.more.editRooms.adapters.room.RoomAdapter
import com.koroyan.smarthome.ui.main_dashboard.more.editRooms.change.ChangeRoomActivity
import com.koroyan.smarthome.ui.main_dashboard.rooms.models.Category
import com.koroyan.smarthome.ui.main_dashboard.rooms.models.Room
import com.koroyan.smarthome.utils.IntentKeys
import kotlinx.android.synthetic.main.activity_edit_rooms.*


class EditRoomsActivity : AppCompatActivity(),OnStatusListener {
    private lateinit var categoryAdapter: CategoryAdapter
    private lateinit var categoryLayout:LinearLayoutManager

    private lateinit var roomAdapter: RoomAdapter
    private lateinit var roomLayout: LinearLayoutManager
    private lateinit var rooms:MutableList<Room>

    private var editRoomsViewModel:EditRoomsViewModel? = null

    private var  category = "In Home"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_rooms)
        init()
    }
    private fun init(){
        setToolBar(getString(R.string.edit_rooms),true)
        rooms = mutableListOf()
        editRoomsViewModel = ViewModelProvider(this)[EditRoomsViewModel::class.java]

        initCategoryAdapter()
        getCategories()

        initRoomAdapter()
        getRooms()
    }

    private fun initCategoryAdapter(){
        editRoomsViewModel = ViewModelProvider(this)[EditRoomsViewModel::class.java]
        editRoomsViewModel!!.setStatusListener(this)
        categoryAdapter =
            CategoryAdapter(
               onCategoryItemClickListener
            )
        categoryLayout = LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)
        categoryRecyclerView.apply {
            layoutManager = categoryLayout
            adapter = categoryAdapter
        }
    }

    private fun initRoomAdapter(){
        roomAdapter = RoomAdapter(onRoomItemClickListener)
        roomLayout = LinearLayoutManager(this)
        roomsRecyclerView.apply {
            layoutManager = roomLayout
            adapter = roomAdapter
        }
    }

    private fun getCategories(){
        editRoomsViewModel!!.categories.observe(this, Observer {
            categoryAdapter.setData(it)
        })
    }

    private fun getRooms(){
        editRoomsViewModel!!.rooms.observe(this, Observer {
            rooms = it
            roomAdapter.setData(mutableListOf())
            roomAdapter.setData(rooms)
        })
    }


    private val onCategoryItemClickListener = object :
        OnItemClickListener {
        override fun onItemClick(position: Int, category: Category) {
            rooms.clear()
            roomAdapter.setData(rooms)
            editRoomsViewModel!!.readRooms(category.name)
            this@EditRoomsActivity.category = category.name
        }

    }
        private val onRoomItemClickListener = object :
        com.koroyan.smarthome.ui.main_dashboard.more.editRooms.adapters.room.OnItemClickListener{
            override fun onItemClick(v: View, room: Room) {
                val popupMenu = PopupMenu(this@EditRoomsActivity, v)
                popupMenu.menuInflater.inflate(R.menu.edit_room_menu, popupMenu.menu)
                popupMenu.setOnMenuItemClickListener { menuItem ->
                    when(menuItem!!.itemId){
                        R.id.menuItemDelete ->{
                            editRoomsViewModel!!.removeRoom(category,room.title)
                            roomAdapter.setData(mutableListOf())
                        }
                        R.id.menuItemChange ->{
                            val intent = Intent(this@EditRoomsActivity,ChangeRoomActivity::class.java)
                            intent.putExtra(IntentKeys.ROOM,room)
                            startActivityForResult(intent,IntentKeys.CHANGE_ROOM_REQUEST)
                        }
                    }
                    true
                }
                popupMenu.gravity = Gravity.CENTER
                popupMenu.show()
            }


        }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode==IntentKeys.CHANGE_ROOM_REQUEST){
            val room = data!!.extras!!.get(IntentKeys.ROOM) as Room
            val oldRoomName = data.extras!!.get(IntentKeys.OLD_ROOM_NAME) as String
            editRoomsViewModel!!.changeRoom(oldRoomName,category,room)
        }
    }




    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            super.onBackPressed()
            super.finish()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSuccess() {
        progressBar.visibility = GONE
        editRoomsViewModel!!.readRooms(category)
    }

    override fun onProgress() {
        progressBar.visibility = VISIBLE
    }

    override fun onFailed(error: String) {
        progressBar.visibility = GONE
        Tools.errorDialog(this,"!!!",error)
    }

}