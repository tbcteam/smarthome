package com.koroyan.smarthome.ui.main_dashboard.more.myprofile

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.koroyan.smarthome.callbacks.OnStatusListener
import com.koroyan.smarthome.network.firebase.Firebase
import com.koroyan.smarthome.network.firebase.OnReadListener
import com.koroyan.smarthome.ui.main_dashboard.more.myprofile.User

class MyProfileViewModel : ViewModel() {

    private val database = FirebaseDatabase.getInstance()
    private val myRef = database.getReference("users")

    private val auth = FirebaseAuth.getInstance()
    private var mUser: FirebaseUser? = null

    private var mStorageRef: StorageReference? = null

    private var onStatusListener: OnStatusListener? = null

    fun setStatusListener(onStatusListener: OnStatusListener) {
        this.onStatusListener = onStatusListener
    }

    init {
        mStorageRef = FirebaseStorage.getInstance().reference
        mUser = auth.currentUser
    }

    private val _user = MutableLiveData<User>().apply {
        Firebase.getUser(object :OnReadListener{
            override fun readUser(user: User) {
                value = user
            }
        })
    }
    val user: LiveData<User> = _user


    fun setData(id:String,image: Uri?, name: String, email: String, phone: String) {
        Firebase.setData(id,image, name, email, phone, onStatusListener)
    }

}