package com.koroyan.smarthome.ui.main_dashboard.more.editRooms.adapters.room

import android.view.View
import com.koroyan.smarthome.ui.main_dashboard.rooms.models.Room

interface OnItemClickListener {
    fun onItemClick(v: View, room: Room)
}