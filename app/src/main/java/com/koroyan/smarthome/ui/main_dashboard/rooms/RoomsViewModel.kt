package com.koroyan.smarthome.ui.main_dashboard.rooms


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.koroyan.smarthome.R
import com.koroyan.smarthome.callbacks.OnStatusListener
import com.koroyan.smarthome.network.firebase.Firebase
import com.koroyan.smarthome.network.firebase.OnReadListener
import com.koroyan.smarthome.ui.main_dashboard.rooms.models.Category
import com.koroyan.smarthome.ui.main_dashboard.rooms.models.Room


class RoomsViewModel : ViewModel() {

    private val _categories = MutableLiveData<ArrayList<Category>>().apply {
        value = arrayListOf(
            Category("In Home", R.mipmap.ic_in_home),
            Category("Home Away", R.mipmap.car),
            Category("Guest Mode", R.mipmap.guest),
            Category("Sleeping Mode", R.mipmap.sleep)
        )
    }
    val categories: LiveData<ArrayList<Category>> = _categories


    private val _rooms = MutableLiveData<MutableList<Room>>().apply {
    }
    val rooms: LiveData<MutableList<Room>> = _rooms

    fun readRooms(category:String){
       Firebase.readRooms(category,object :OnReadListener{
           override fun roomRead(rooms: MutableList<Room>) {
               _rooms.value = rooms
           }

           override fun changed() {
               readRooms(category)
           }

       })
    }

    fun checkDevice(category:String,room:Room,onStatusListener: OnStatusListener){
        Firebase.checkDevice(category,room,onStatusListener)
    }

}