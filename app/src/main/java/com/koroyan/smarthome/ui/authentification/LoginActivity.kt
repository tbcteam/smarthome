package com.koroyan.smarthome.ui.authentification

import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.marginBottom
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.koroyan.smarthome.MyReceiver
import com.koroyan.smarthome.R
import com.koroyan.smarthome.data.preferencedata.SharedPreferences.ID
import com.koroyan.smarthome.data.preferencedata.SharedPreferences.MAIL_FOR_FIREBASE
import com.koroyan.smarthome.data.preferencedata.SharedPreferences.saveString
import com.koroyan.smarthome.extensions.isEmailValid
import com.koroyan.smarthome.extensions.setToolBar
import com.koroyan.smarthome.tools.Tools
import com.koroyan.smarthome.ui.main_dashboard.DashboardActivity
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.layout_toolbar.*


class LoginActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private val myReceiver by lazy {
        MyReceiver()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        auth = FirebaseAuth.getInstance()
        setToolBar(getString(R.string.sign_in),false)
        init()
    }

    private fun init() {
        emailAddressEditText.isEmailValid(this)
        registerNowTextView.setOnClickListener {
            startActivity(Intent(this, SignUpActivity::class.java))
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        }
    }

    fun rememberMe(view: View) {
        if (rememberMeImage.tag == "0") {
            rememberMeImage.setImageResource(R.mipmap.ic_remember_me)
            rememberMeImage.tag = "1"
        } else {
            rememberMeImage.setImageResource(R.mipmap.ic_uncheck_remember_me)
            rememberMeImage.tag = "0"
        }
    }

    fun signInButton(view: View) {
        val email = emailAddressEditText.text.toString()
        val password = passwordEditText.text.toString()

        if (rememberMeImage.tag == "1") {
            saveString(
                com.koroyan.smarthome.data.preferencedata.SharedPreferences.IS_REMEMBERED,
                "Remembered"
            )
        } else {
            saveString(
                com.koroyan.smarthome.data.preferencedata.SharedPreferences.IS_REMEMBERED,
                "Not remembered"
            )
        }

        if (email.isEmpty() || password.isEmpty()) {
            Tools.errorDialog(
                this,
                "Incorrect Request",
                "Please fill all fields"
            )
        } else {
            when {
                emailAddressEditText.tag == "0" -> Tools.errorDialog(
                    this,
                    "Incorrect Request",
                    "Email is incorrect"
                )
                password.length < 6 -> {
                    Tools.errorDialog(
                        this,
                        "Incorrect Request",
                        "Password length must be 6 or more characters."
                    )
                }
                else -> {
                    val id = email.replace('.', 'g').toString()
                    saveString(MAIL_FOR_FIREBASE,id)
                    signIn(email, password)
                }
            }
        }
    }

    private fun signIn(email: String, password: String) {
        progressBar.visibility = VISIBLE
        imageButtonNext.isClickable = false
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val user = auth.currentUser
                    updateMyUI(user)
                } else {
                    Toast.makeText(
                        baseContext, "LogIn failed." + task.exception,
                        Toast.LENGTH_LONG
                    ).show()
                    updateMyUI(null)
                }
            }
    }

    private fun updateMyUI(currentUser: FirebaseUser?) {
        progressBar.visibility = GONE
        imageButtonNext.isClickable = true
        if (currentUser != null) {
            if (currentUser.isEmailVerified) {
                saveString(ID, currentUser.uid)
                startActivity(
                    Intent(
                        this,
                        DashboardActivity::class.java
                    ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                )
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)

            } else {
                Toast.makeText(
                    baseContext, "Please verify you email address.",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }
    override fun onResume() {
        super.onResume()
        val intentFilter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        registerReceiver(myReceiver, intentFilter)
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(myReceiver)
    }

}