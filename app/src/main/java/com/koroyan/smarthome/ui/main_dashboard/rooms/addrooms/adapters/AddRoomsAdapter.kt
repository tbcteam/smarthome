package com.koroyan.smarthome.ui.main_dashboard.rooms.addrooms.adapters

import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.koroyan.smarthome.R
import com.koroyan.smarthome.ui.main_dashboard.rooms.models.Room
import kotlinx.android.synthetic.main.layout_add_rooms.view.*
import kotlinx.android.synthetic.main.layout_rooms_add_room.view.*
import kotlinx.android.synthetic.main.layout_rooms_add_room.view.cardBackgroundImageView

class AddRoomsAdapter(private var addImageClickListener: AddImageClickListener):RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object{
        const val ROOMS = 1
        const val ADD_ROOMS = 2
    }

    var selectedRoomName = ""

    private var roomsModel:MutableList<Room> = mutableListOf()

    inner class RoomsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        lateinit var model: Room
        fun onBind(){
            model = roomsModel[adapterPosition]
            with(itemView){
                Glide.with(this).load(model.background).placeholder(R.drawable.ic_category_card_view_default).into(cardBackgroundImageView)
                roomTitleTextView.text = model.title
            }
        }
    }

    inner class AddRoomViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        lateinit var model: Room
        fun onBind(){
            model = roomsModel[adapterPosition]
            with(itemView){
                Glide.with(this).load(model.background).placeholder(R.drawable.ic_category_card_view_default).into(cardBackgroundImageView)
                roomNameEditText.text = Editable.Factory().newEditable(selectedRoomName)
                roomNameEditText.addTextChangedListener(object:TextWatcher{
                    override fun afterTextChanged(s: Editable?) {

                    }

                    override fun beforeTextChanged(
                        s: CharSequence?,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {

                    }

                    override fun onTextChanged(
                        s: CharSequence?,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {
                        selectedRoomName = s.toString()
                    }

                })
            }

            itemView.coosBackgroundFrameButton.setOnClickListener{
                addImageClickListener.coosImage(it)
            }
            itemView.addRoomButton.setOnClickListener {
                addImageClickListener.addImage()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == ROOMS)
            return RoomsViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.layout_rooms_add_room,parent,false))
        return AddRoomViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.layout_add_rooms,parent,false))
    }

    override fun getItemCount(): Int = roomsModel.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is RoomsViewHolder) {
            holder.onBind()
        }else if (holder is AddRoomViewHolder){
                holder.onBind()
            }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == roomsModel.size-1)
            ADD_ROOMS
        else
            ROOMS
    }

    fun setData(roomsModel: MutableList<Room>){
        this.roomsModel = roomsModel
        notifyDataSetChanged()
    }
}