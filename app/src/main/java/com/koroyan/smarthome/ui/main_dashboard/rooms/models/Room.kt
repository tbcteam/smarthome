package com.koroyan.smarthome.ui.main_dashboard.rooms.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Room(var title: String = "",
           var temperature: Float = 0.0f,
           var background: String = "",
           var cameraDevice: Boolean = false,
           var unitDevice:Boolean = false,
           var musicDevice:Boolean = false,
           var lampDevice:Boolean = false,
           var liveCameraUrl:String = "empty"):Parcelable