package com.koroyan.smarthome.ui.main_dashboard.music


import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.koroyan.smarthome.R
import com.koroyan.smarthome.network.CustomCallback
import com.koroyan.smarthome.network.Endpoints
import com.koroyan.smarthome.network.HttpRequest
import com.koroyan.smarthome.ui.main_dashboard.music.band.BandInfoActivity
import com.koroyan.smarthome.ui.main_dashboard.music.band.BandModel
import com.koroyan.smarthome.ui.main_dashboard.music.band.BandsRecyclerViewAdapter
import com.koroyan.smarthome.ui.main_dashboard.music.band.CustomListener
import kotlinx.android.synthetic.main.power_fragment.*
import kotlinx.android.synthetic.main.power_fragment.view.*
import org.json.JSONArray
import org.json.JSONObject

class PowerFragment : Fragment() {

    private var itemView: View? = null
    private val bands = mutableListOf<BandModel>()
    private lateinit var adapter: BandsRecyclerViewAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (itemView == null)
            itemView = inflater.inflate(R.layout.power_fragment, container, false)
        getBands()
        return itemView
    }

    private fun init() {
        itemView!!.findViewById<RecyclerView>(R.id.bandsRecyclerView).layoutManager =
            LinearLayoutManager(activity)
        adapter =
            BandsRecyclerViewAdapter(
                bands,
                object :
                    CustomListener {
                    override fun onClick(position: Int) {
                        val intent = Intent(context, BandInfoActivity::class.java)
                        val band = bands[position]
                        intent.putExtra("band", band)
                        startActivity(intent)
                    }

                })

        itemView!!.bandsRecyclerView.adapter = adapter

        itemView!!.swipeRefreshLayout.setOnRefreshListener {
            swipeRefreshLayout.isRefreshing = true
            refresh()

            Handler().postDelayed({
                swipeRefreshLayout.isRefreshing = false
                getBands()
                adapter.notifyDataSetChanged()
            }, 1500)
        }
    }

    private fun refresh() {
        bands.clear()
        adapter.notifyDataSetChanged()
    }

    private fun getBands() {
        HttpRequest.getRequest(Endpoints.MAIN_INFO, object : CustomCallback {
            override fun onFailure(error: String) {
                Toast.makeText(context, error, Toast.LENGTH_LONG).show()
            }

            override fun onResponse(response: String) {
                val jsonArray = JSONArray(response)

                (0 until jsonArray.length()).forEach {
                    val jsonElement = jsonArray.get(it) as JSONObject
                    val band = Gson().fromJson(jsonElement.toString(), BandModel::class.java)
                    bands.add(band)
                }

                init()

            }

        })

    }

}