package com.koroyan.smarthome.ui.main_dashboard.rooms.addrooms

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.koroyan.smarthome.R
import com.koroyan.smarthome.tools.Tools
import com.koroyan.smarthome.ui.main_dashboard.rooms.addrooms.adapters.AddImageClickListener
import com.koroyan.smarthome.ui.main_dashboard.rooms.addrooms.adapters.AddRoomsAdapter
import com.koroyan.smarthome.callbacks.OnStatusListener
import com.koroyan.smarthome.extensions.setToolBar
import com.koroyan.smarthome.image_chooser.EasyImage
import com.koroyan.smarthome.image_chooser.ImageChooserUtils
import com.koroyan.smarthome.image_chooser.MediaFile
import com.koroyan.smarthome.image_chooser.MediaSource
import com.koroyan.smarthome.ui.main_dashboard.rooms.models.Room
import com.koroyan.smarthome.utils.IntentKeys
import com.koroyan.smarthome.utils.IntentKeys.UPDATE_IMAGE_REQUEST_DATABASE
import kotlinx.android.synthetic.main.activity_add_rooms.*

class AddRoomsActivity : AppCompatActivity(),
    OnStatusListener, View.OnClickListener {

    private var filePath: Uri? = null
    private var category = "In Home"

    private lateinit var addRoomsAdapter: AddRoomsAdapter
    private lateinit var layout: LinearLayoutManager
    private lateinit var rooms: MutableList<Room>

    private var addRoomsViewModel: AddRoomsViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_rooms)
        setToolBar(getString(R.string.add_rooms), true)
        init()
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            super.onBackPressed()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        }
        return super.onOptionsItemSelected(item)
    }

    private fun init() {
        rooms = mutableListOf()
        addRoomsViewModel = ViewModelProvider(this)[AddRoomsViewModel::class.java]
        addRoomsViewModel!!.setOnStatusListener(this)
        category = intent.extras!!.getString(IntentKeys.CATEGORY, "in Home")!!
        initAddRoomsAdapter()
        nextFloatingButton.setOnClickListener(this)
    }

    private fun initAddRoomsAdapter() {
        addRoomsAdapter = AddRoomsAdapter(addImageClickListener)
        layout = LinearLayoutManager(this)
        recyclerView.apply {
            adapter = addRoomsAdapter
            layoutManager = layout
        }
        addRoomsViewModel?.rooms?.observe(this, Observer {
            addRoomsAdapter.setData(mutableListOf())
            rooms = it
            rooms.add(rooms.size, Room("", 0.0f, "empty"))
            addRoomsAdapter.setData(rooms)
            recyclerView.scrollToPosition(rooms.size - 1)
        })
        addRoomsViewModel!!.readRooms(category)
    }

    private val addImageClickListener = object : AddImageClickListener {
        override fun coosImage(v: View) {
            choosePhoto()
        }

        override fun addImage() {
            addRoomsViewModel!!.uploadPost(filePath, category, addRoomsAdapter.selectedRoomName)
        }
    }

    override fun onSuccess() {
        progressBar.visibility = GONE
        addRoomsViewModel!!.readRooms(category)
        clearData()
    }

    override fun onProgress() {
        progressBar.visibility = VISIBLE
    }

    override fun onFailed(error: String) {
        progressBar.visibility = GONE
        Tools.errorDialog(this, "!!!", error)
        clearData()
    }

    private fun clearData() {
        filePath = null
        addRoomsAdapter.selectedRoomName = ""
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.nextFloatingButton -> {
                finish()
            }
        }
    }

    fun choosePhoto() {
        if (ImageChooserUtils.hasReadExternalStoragePermission() && ImageChooserUtils.hasWriteExternalStoragePermission() && ImageChooserUtils.hasCameraPermission()) {
            ImageChooserUtils.chooseResource(this)
        } else {
            ImageChooserUtils.requestPermission(this)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == ImageChooserUtils.PERMISSIONS_REQUEST) {
            if (grantResults.size > 2) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    ImageChooserUtils.chooseResource(this)
                }
            }
        }
    }

    //    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if (resultCode == Activity.RESULT_OK) {
//            filePath = data!!.data
//            if (requestCode == ADD_IMAGE_REQUEST) {
//                rooms[rooms.size - 1].title = addRoomsAdapter.selectedRoomName
//                rooms[rooms.size - 1].background = filePath!!.toString()
//                addRoomsAdapter.setData(rooms)
//            } else if (requestCode == UPDATE_IMAGE_REQUEST_DATABASE) {
//                addRoomsViewModel!!.uploadPost(
//                    filePath,
//                    category,
//                    addRoomsAdapter.selectedRoomName
//                )
//            }
//        }
//    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        ImageChooserUtils.easyImage.handleActivityResult(
            requestCode,
            resultCode,
            data,
            this,
            object : EasyImage.Callbacks {
                override fun onMediaFilesPicked(
                    imageFiles: Array<MediaFile>,
                    source: MediaSource
                ) {
                    if (imageFiles.isNotEmpty()) {
                        filePath = imageFiles[0].uri
                        rooms[rooms.size - 1].title = addRoomsAdapter.selectedRoomName
                        rooms[rooms.size - 1].background = filePath!!.toString()
                        addRoomsAdapter.setData(rooms)
                    }

                }

                override fun onImagePickerError(
                    error: Throwable,
                    source: MediaSource
                ) {
                }

                override fun onCanceled(source: MediaSource) {
                }
            })
        if (requestCode == UPDATE_IMAGE_REQUEST_DATABASE) {
            addRoomsViewModel!!.uploadPost(
                filePath,
                category,
                addRoomsAdapter.selectedRoomName
            )
        }
    }

}