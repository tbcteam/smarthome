package com.koroyan.smarthome.ui.main_dashboard.more.editRooms


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.koroyan.smarthome.R
import com.koroyan.smarthome.callbacks.OnStatusListener
import com.koroyan.smarthome.network.firebase.Firebase
import com.koroyan.smarthome.network.firebase.OnReadListener
import com.koroyan.smarthome.ui.main_dashboard.rooms.models.Category
import com.koroyan.smarthome.ui.main_dashboard.rooms.models.Room

class EditRoomsViewModel:ViewModel() {
    private val database = FirebaseDatabase.getInstance()
    private val myRef = database.getReference("rooms")
    private val user = FirebaseAuth.getInstance().currentUser

    private var mStorageRef: StorageReference? = null

    init {
        mStorageRef = FirebaseStorage.getInstance().reference
    }

    private var onStatusListener:OnStatusListener? = null

    fun setStatusListener(onStatusListener: OnStatusListener){
        this.onStatusListener = onStatusListener
    }

    private val _categories = MutableLiveData<ArrayList<Category>>().apply {
        value = arrayListOf(
            Category("In Home", R.mipmap.ic_in_home),
            Category("Home Away", R.mipmap.car),
            Category("Guest Mode", R.mipmap.guest),
            Category("Sleeping Mode", R.mipmap.sleep)
        )
    }
    val categories: LiveData<ArrayList<Category>> = _categories

    private val _rooms = MutableLiveData<MutableList<Room>>().apply {
        value = mutableListOf()
    }
    val rooms: LiveData<MutableList<Room>> = _rooms


    fun readRooms(category: String) {
       Firebase.readRooms(category,object :OnReadListener{
           override fun roomRead(rooms: MutableList<Room>) {
               _rooms.value = rooms
           }
       })
    }

    fun removeRoom(category:String,room:String){
        Firebase.removeRoom(category,room)
        readRooms(category)
    }

    fun changeRoom(oldRoomName:String, category:String, room: Room) {
       Firebase.changePost(oldRoomName,category,room,onStatusListener)
        readRooms(category)
    }

}