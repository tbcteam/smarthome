package com.koroyan.smarthome.ui.main_dashboard.more.help.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.koroyan.smarthome.R
import com.koroyan.smarthome.databinding.HelpTextRecyclerviewLayoutBinding
import com.koroyan.smarthome.extensions.isClicked
import com.koroyan.smarthome.ui.main_dashboard.more.help.HelpTextsModel

class HelpRecyclerViewAdapter(
    private val helpTexts: MutableList<HelpTextsModel>
) :
    RecyclerView.Adapter<HelpRecyclerViewAdapter.ViewHolder>(){

    private var selectedPosition:Int = -1

    inner class ViewHolder(private val binding: HelpTextRecyclerviewLayoutBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        fun onBind() {
            binding.helpTextsModel
            binding.helpTextsModel = helpTexts[adapterPosition]
            binding.descriptionTextView.isClicked = selectedPosition == adapterPosition
            binding.helpTextImageView.isClicked = selectedPosition == adapterPosition
            binding.itemView.setOnClickListener(this)
        }

        override fun onClick(p0: View?) {
            selectedPosition = adapterPosition
            notifyDataSetChanged()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: HelpTextRecyclerviewLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.help_text_recyclerview_layout,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = helpTexts.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.onBind()

}