package com.koroyan.smarthome.ui.main_dashboard.rooms.addrooms


import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.koroyan.smarthome.callbacks.OnStatusListener
import com.koroyan.smarthome.network.firebase.Firebase
import com.koroyan.smarthome.network.firebase.OnReadListener
import com.koroyan.smarthome.ui.main_dashboard.rooms.models.Room

class AddRoomsViewModel: ViewModel() {
    private val database = FirebaseDatabase.getInstance()
    private val myRef = database.getReference("rooms")

    private var mStorageRef: StorageReference? = null

    init {
        mStorageRef = FirebaseStorage.getInstance().reference
    }

    private var onStatusListener: OnStatusListener? = null

    fun setOnStatusListener(onStatusListener: OnStatusListener) {
        this.onStatusListener = onStatusListener
    }

    private val _rooms = MutableLiveData<MutableList<Room>>().apply {
        value = arrayListOf()
    }
    val rooms: LiveData<MutableList<Room>> = _rooms

    fun readRooms(category: String) {
       Firebase.readRooms(category,object :OnReadListener{
           override fun roomRead(rooms: MutableList<Room>) {
               _rooms.value = rooms
           }

       })
    }


    fun uploadPost(filePath: Uri?, category:String, roomName: String) {
         Firebase.uploadPost(filePath,category,roomName,onStatusListener)
     }

}