package com.koroyan.smarthome.ui.main_dashboard.more.editRooms.change

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.view.MenuItem
import android.view.View
import com.bumptech.glide.Glide
import com.koroyan.smarthome.R
import com.koroyan.smarthome.extensions.setToolBar
import com.koroyan.smarthome.image_chooser.EasyImage
import com.koroyan.smarthome.image_chooser.ImageChooserUtils
import com.koroyan.smarthome.image_chooser.MediaFile
import com.koroyan.smarthome.image_chooser.MediaSource
import com.koroyan.smarthome.tools.Tools
import com.koroyan.smarthome.ui.main_dashboard.rooms.models.Room
import com.koroyan.smarthome.utils.IntentKeys
import kotlinx.android.synthetic.main.activity_change_room.*

class ChangeRoomActivity : AppCompatActivity(), View.OnClickListener {

    private var filePath: Uri? = null
    private var room: Room? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_room)
        init()
    }

    private fun init() {
        setToolBar(getString(R.string.change_room), true)
        room = intent.getParcelableExtra(IntentKeys.ROOM)!!
        Glide.with(this).load(room!!.background)
            .placeholder(R.drawable.ic_category_card_view_default).into(cardBackgroundImageView)
        roomNameEditText.text = Editable.Factory().newEditable(room!!.title)

        changeRoomButton.setOnClickListener(this)
        coosBackgroundFrameButton.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.changeRoomButton -> {
                val name = roomNameEditText.text.toString()
                if (name.isEmpty()) {
                    Tools.errorDialog(this, "Failed Request", "room name is empty")
                    return
                }
                filePath = if (filePath == null) Uri.EMPTY else filePath
                val intent = intent
                intent.putExtra(IntentKeys.OLD_ROOM_NAME, room!!.title)
                room!!.title = name
                room!!.background = filePath.toString()
                intent.putExtra(IntentKeys.ROOM, room)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
            R.id.coosBackgroundFrameButton -> {
                choosePhoto()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            super.onBackPressed()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        }
        return super.onOptionsItemSelected(item)
    }


    private fun choosePhoto() {
        if (ImageChooserUtils.hasReadExternalStoragePermission() && ImageChooserUtils.hasWriteExternalStoragePermission() && ImageChooserUtils.hasCameraPermission()) {
            ImageChooserUtils.chooseResource(this)
        } else {
            ImageChooserUtils.requestPermission(this)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == ImageChooserUtils.PERMISSIONS_REQUEST) {
            if (grantResults.size > 2) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    ImageChooserUtils.chooseResource(this)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        ImageChooserUtils.easyImage.handleActivityResult(
            requestCode,
            resultCode,
            data,
            this,
            object : EasyImage.Callbacks {
                override fun onMediaFilesPicked(
                    imageFiles: Array<MediaFile>,
                    source: MediaSource
                ) {
                    if (imageFiles.isNotEmpty()) {
                        filePath = imageFiles[0].uri
                        Glide.with(this@ChangeRoomActivity).load(filePath)
                            .placeholder(R.drawable.ic_category_card_view_default)
                            .into(cardBackgroundImageView)
                    }

                }

                override fun onImagePickerError(
                    error: Throwable,
                    source: MediaSource
                ) {
                }

                override fun onCanceled(source: MediaSource) {
                }
            })

    }
}