package com.koroyan.smarthome.ui.main_dashboard.rooms.adapters.categories

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.koroyan.smarthome.R
import com.koroyan.smarthome.ui.main_dashboard.rooms.models.Category
import kotlinx.android.synthetic.main.category_recyclerview_item.view.*

class CategoryRecyclerViewAdapter(private var onItemClickListener: OnItemClickListener):RecyclerView.Adapter<CategoryRecyclerViewAdapter.ViewHolder>() {

    private var categories:ArrayList<Category> = arrayListOf()
    var selectedPosition = 0

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),View.OnClickListener{
        private lateinit var model:Category
        fun onBind(){
            model = categories[adapterPosition]
            itemView.setOnClickListener(this)
            itemView.categoryIconImageView.setImageResource(model.icon)
            itemView.categoryTitleTextView.text = model.name
            if (adapterPosition == selectedPosition) {
                itemView.cardBackgroundImageView.setImageResource(R.drawable.ic_category_card_view)
                onItemClickListener.onItemClick(adapterPosition, model)
            }
            else {
                itemView.cardBackgroundImageView.setImageResource(R.drawable.ic_category_card_view_default)
            }
        }

        override fun onClick(v: View?) {
            val position = selectedPosition
            selectedPosition = adapterPosition
            notifyItemChanged(position)
            notifyItemChanged(selectedPosition)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val holder = LayoutInflater.from(parent.context).inflate(R.layout.category_recyclerview_item,parent,false)
        return ViewHolder(holder)
    }

    override fun getItemCount(): Int = categories.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    fun setData(categories:ArrayList<Category>){
        this.categories = categories
        notifyDataSetChanged()
    }
}