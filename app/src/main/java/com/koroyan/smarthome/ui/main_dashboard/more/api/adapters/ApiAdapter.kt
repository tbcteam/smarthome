package com.koroyan.smarthome.ui.main_dashboard.more.api.adapters

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.koroyan.smarthome.App
import com.koroyan.smarthome.R
import com.koroyan.smarthome.extensions.setGradientColor
import com.koroyan.smarthome.ui.main_dashboard.more.api.models.ApiMap
import kotlinx.android.synthetic.main.layout_api_adapter.view.*


class ApiAdapter : RecyclerView.Adapter<ApiAdapter.ViewHolder>() {
    private var apis: MutableList<ApiMap> = mutableListOf()

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var model: ApiMap
        fun onBind() {
            model = apis[adapterPosition]
            with(itemView){
                keyTextView.text = model.key
                valueTextView.text = model.value
            }
            itemView.copyTextIV.setOnClickListener {
                val clipboard: ClipboardManager? =
                    App.instance.context!!.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager?
                val clip = ClipData.newPlainText("text", itemView.valueTextView.text)
                clipboard!!.setPrimaryClip(clip)
                Toast.makeText(itemView.context,"copied",Toast.LENGTH_SHORT).show()
            }
            itemView.keyTextView.setGradientColor()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.layout_api_adapter, parent, false)
        )
    }

    override fun getItemCount(): Int = apis.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    fun setData(apis:MutableList<ApiMap>){
        this.apis = apis
        notifyDataSetChanged()
    }
}