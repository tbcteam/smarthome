package com.koroyan.smarthome.ui.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.firebase.auth.FirebaseAuth
import com.koroyan.smarthome.R
import com.koroyan.smarthome.data.preferencedata.SharedPreferences
import com.koroyan.smarthome.ui.main_dashboard.DashboardActivity
import com.koroyan.smarthome.ui.authentification.LoginActivity

class SplashScreenActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private var account: GoogleSignInAccount? = null
    private var mDelayHandler: Handler? = null

    companion object {
        const val SPLASH_DELAY: Long = 2000
    }

    private val mRunnable: Runnable = Runnable {
        var user = auth.currentUser

        if (SharedPreferences.getString(SharedPreferences.IS_REMEMBERED).equals("Not remembered"))
            user = null
        SharedPreferences.removeString(SharedPreferences.IS_REMEMBERED)

        if (user != null)
            startActivity(
                Intent(
                    this,
                    DashboardActivity::class.java
                ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            )
        else
            startActivity(
                Intent(
                    this,
                    LoginActivity::class.java
                ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        auth = FirebaseAuth.getInstance()
        init()
    }

    private fun init() {
        mDelayHandler = Handler()
        account = GoogleSignIn.getLastSignedInAccount(this)
    }

    public override fun onResume() {
        mDelayHandler!!.postDelayed(
            mRunnable,
            SPLASH_DELAY
        )
        super.onResume()
    }

    public override fun onStop() {
        if (mDelayHandler != null) {
            mDelayHandler!!.removeCallbacks(mRunnable)
        }
        super.onStop()
    }

}



