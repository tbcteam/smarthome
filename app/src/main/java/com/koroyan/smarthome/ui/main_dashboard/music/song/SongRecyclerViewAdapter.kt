package com.koroyan.smarthome.ui.main_dashboard.music.song

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.koroyan.smarthome.R
import com.koroyan.smarthome.extensions.setGradientColor
import com.koroyan.smarthome.ui.main_dashboard.music.band.CustomListener
import com.koroyan.smarthome.ui.main_dashboard.music.band.BandModel
import kotlinx.android.synthetic.main.song_recyclerview_layout.view.*

class SongRecyclerViewAdapter(
    private val songs: MutableList<BandModel.SongModel>,
    private val customListener: CustomListener
) : RecyclerView.Adapter<SongRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.song_recyclerview_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount(): Int {
        return songs.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var song: BandModel.SongModel

        fun onBind() {
            song = songs[adapterPosition]

            itemView.apply {
                if (song.title != "Songs not found") {
                    this.setOnClickListener {
                        customListener.onClick(adapterPosition)
                    }
                }
                titleTextView.text = song.title
                titleTextView.setGradientColor()
            }
        }

    }

}