package com.koroyan.smarthome.ui.authentification

import android.content.Intent
import android.content.IntentFilter
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.koroyan.smarthome.MyReceiver
import com.koroyan.smarthome.R
import com.koroyan.smarthome.callbacks.OnStatusListener
import com.koroyan.smarthome.extensions.isEmailValid
import com.koroyan.smarthome.extensions.setToolBar
import com.koroyan.smarthome.image_chooser.*
import com.koroyan.smarthome.network.firebase.Firebase
import com.koroyan.smarthome.tools.Tools
import kotlinx.android.synthetic.main.activity_sign_up.*


class SignUpActivity : AppCompatActivity(), OnStatusListener {

    private val myReceiver by lazy {
        MyReceiver()
    }

    private var filePath: Uri = Uri.EMPTY
    private lateinit var auth: FirebaseAuth
    private val database = FirebaseDatabase.getInstance()
    private val myUser = database.getReference("users")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        auth = FirebaseAuth.getInstance()
        init()
    }

    private fun init() {
        setToolBar(getString(R.string.sign_up), true)
        emailAddressEditText.isEmailValid(this)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            super.onBackPressed()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        }
        return super.onOptionsItemSelected(item)
    }

    fun signUp(view: View) {
        val enteredName = fullNameEditText.text.toString()
        val enteredEmail = emailAddressEditText.text.toString()
        val enteredPassword = createPasswordEditText.text.toString()
        val enteredConfirmPassword = confirmPasswordEditText.text.toString()

        if (enteredName.isEmpty() || enteredEmail.isEmpty() || enteredPassword.isEmpty() || enteredConfirmPassword.isEmpty()) {
            Tools.errorDialog(
                this,
                "Incorrect Request",
                "Please fill all fields"
            )
        } else {
            when {
                emailAddressEditText.tag == "0" -> {
                    Tools.errorDialog(
                        this,
                        "Incorrect Request",
                        "Email is incorrect"
                    )
                }
                enteredPassword.length < 6 -> {
                    Tools.errorDialog(
                        this,
                        "Incorrect Request",
                        "Password length must be 6 or more characters."
                    )
                }
                enteredPassword != enteredConfirmPassword -> {
                    Tools.errorDialog(
                        this,
                        "Incorrect Request",
                        "Passwords do not match!"
                    )
                }
                else -> {
                    createUser(enteredEmail, enteredPassword, enteredName)
                }
            }
        }
    }

    private fun createUser(enteredEmail: String, enteredPassword: String, enteredName: String) {
        auth.createUserWithEmailAndPassword(enteredEmail, enteredPassword)
            .addOnCompleteListener(this)
            { task ->
                if (task.isSuccessful) {
                    val user = auth.currentUser
                    val id = enteredEmail.replace('.', 'g').toString()
                    com.koroyan.smarthome.data.preferencedata.SharedPreferences.saveString(com.koroyan.smarthome.data.preferencedata.SharedPreferences.MAIL_FOR_FIREBASE,id)
                    Firebase.setData(id,filePath, enteredName, enteredEmail, "", this)
                    user!!.sendEmailVerification()
                        .addOnCompleteListener {
                            if (task.isSuccessful) {
                                super.onBackPressed()
                                overridePendingTransition(
                                    android.R.anim.fade_in,
                                    android.R.anim.fade_out
                                )
                            }
                        }
                    Toast.makeText(
                        baseContext, "Email verification link was sent to your email.",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    Toast.makeText(
                        baseContext, "Sign Up failed." + task.exception,
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
    }

    fun choosePhoto(view: View) {
        if (ImageChooserUtils.hasReadExternalStoragePermission() && ImageChooserUtils.hasWriteExternalStoragePermission() && ImageChooserUtils.hasCameraPermission()) {
            ImageChooserUtils.chooseResource(this)
        } else {
            ImageChooserUtils.requestPermission(this)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == ImageChooserUtils.PERMISSIONS_REQUEST) {
            if (grantResults.size > 2) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    ImageChooserUtils.chooseResource(this)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        ImageChooserUtils.easyImage.handleActivityResult(
            requestCode,
            resultCode,
            data,
            this,
            object : EasyImage.Callbacks {
                override fun onMediaFilesPicked(
                    imageFiles: Array<MediaFile>,
                    source: MediaSource
                ) {
                    if (imageFiles.isNotEmpty()) {
                        Glide.with(applicationContext).load(imageFiles[0].uri).into(cameraImageView)
                        filePath = imageFiles[0].uri
                    }
                }

                override fun onImagePickerError(
                    error: Throwable,
                    source: MediaSource
                ) {
                }

                override fun onCanceled(source: MediaSource) {
                }
            })
    }

    override fun onSuccess() {
        signUpProgressBar.visibility = View.GONE
    }

    override fun onProgress() {
        signUpProgressBar.visibility = View.VISIBLE
    }

    override fun onFailed(error: String) {
        signUpProgressBar.visibility = View.GONE
    }

    override fun onResume() {
        super.onResume()
        val intentFilter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        registerReceiver(myReceiver, intentFilter)
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(myReceiver)
    }


}