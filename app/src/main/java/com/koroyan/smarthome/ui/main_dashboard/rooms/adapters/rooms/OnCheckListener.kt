package com.koroyan.smarthome.ui.main_dashboard.rooms.adapters.rooms

import android.view.View
import com.koroyan.smarthome.ui.main_dashboard.rooms.models.Room

interface OnCheckListener {
    fun onCheck(v: View,room:Room)
}