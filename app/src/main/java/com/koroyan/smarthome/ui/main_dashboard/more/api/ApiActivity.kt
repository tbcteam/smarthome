package com.koroyan.smarthome.ui.main_dashboard.more.api

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.koroyan.smarthome.R
import com.koroyan.smarthome.extensions.setToolBar
import com.koroyan.smarthome.ui.main_dashboard.more.api.adapters.ApiAdapter
import com.koroyan.smarthome.ui.main_dashboard.more.api.models.ApiMap
import kotlinx.android.synthetic.main.activity_api.*


class ApiActivity : AppCompatActivity() {
    private var apiViewModel: ApiViewModel? = null
    lateinit var apiAdapter: ApiAdapter
    lateinit var apiLayout: LinearLayoutManager
    lateinit var apis: MutableList<ApiMap>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_api)
        init()
    }

    private fun init() {
        setToolBar(getString(R.string.api), true)
        apis = mutableListOf()
        apiViewModel = ViewModelProvider(this)[ApiViewModel::class.java]
        apiAdapter = ApiAdapter()
        apiLayout = LinearLayoutManager(this)
        recyclerView.apply {
            layoutManager = apiLayout
            adapter = apiAdapter
        }
        getData()
    }

    private fun getData() {
        apiViewModel?.api?.observe(this, Observer {
            apiAdapter.setData(it)
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            super.onBackPressed()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        }
        return super.onOptionsItemSelected(item)
    }
}