package com.koroyan.smarthome.ui.main_dashboard.more.contactus

import androidx.lifecycle.ViewModel
import com.koroyan.smarthome.callbacks.OnStatusListener
import com.koroyan.smarthome.network.firebase.Firebase
import com.koroyan.smarthome.ui.main_dashboard.more.contactus.model.Message

class ContactUsViewModel:ViewModel() {

    fun sendMessageToAdmin(message: Message,onStatusListener: OnStatusListener?){
        Firebase.sendMessageToAdmin(message,onStatusListener)
    }
}