package com.koroyan.smarthome.ui.main_dashboard.more.editRooms.adapters.category

import com.koroyan.smarthome.ui.main_dashboard.rooms.models.Category

interface OnItemClickListener {
    fun onItemClick(position: Int, category: Category)
}