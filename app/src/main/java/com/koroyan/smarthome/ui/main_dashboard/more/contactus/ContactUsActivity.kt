package com.koroyan.smarthome.ui.main_dashboard.more.contactus

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.util.Log.d
import android.view.MenuItem
import android.view.View
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.koroyan.smarthome.R
import com.koroyan.smarthome.callbacks.OnStatusListener
import com.koroyan.smarthome.extensions.setToolBar
import com.koroyan.smarthome.network.firebase.Firebase
import com.koroyan.smarthome.tools.Tools
import com.koroyan.smarthome.ui.main_dashboard.more.contactus.model.Message
import kotlinx.android.synthetic.main.activity_contact_us.*

class ContactUsActivity : AppCompatActivity(), View.OnClickListener {
    private var contactUsViewModel: ContactUsViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_us)
        init()
    }

    private fun init() {
        setToolBar(getString(R.string.contact_us), true)
        contactUsViewModel = ViewModelProvider(this)[ContactUsViewModel::class.java]
        sendButton.setOnClickListener(this)
        callButton.setOnClickListener(this)
        emailButton.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.sendButton -> {
                sendFirebase()
            }
            R.id.callButton -> {
                call()
            }
            R.id.emailButton -> {
                email()
            }
        }
    }


    private val onStatusListener = object : OnStatusListener {
        override fun onSuccess() {
            progressBar.visibility = INVISIBLE
            sendButton.isClickable = true
            Toast.makeText(this@ContactUsActivity, "Message was sent", Toast.LENGTH_SHORT).show()
        }

        override fun onProgress() {
            progressBar.visibility = VISIBLE
            messageEditText.text = Editable.Factory().newEditable("")
            sendButton.isClickable = false
        }

        override fun onFailed(error: String) {
            progressBar.visibility = INVISIBLE
            sendButton.isClickable = true
            Tools.errorDialog(this@ContactUsActivity, "!!!", error)
        }
    }

    private fun sendFirebase() {
        contactUsViewModel!!.sendMessageToAdmin(
            Message(
                messageEditText.text.toString(),
                Firebase.mUser?.email.toString()
            ),
            onStatusListener
        )
    }

    private fun call() {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:+995555555555")
        startActivity(intent)
    }

    private fun email() {
        val mailIntent = Intent(Intent.ACTION_SEND).apply {
            type = "text/plain"
            putExtra(Intent.EXTRA_EMAIL, arrayOf("jon@example.com"))
            putExtra(Intent.EXTRA_SUBJECT, "Email subject")
            putExtra(Intent.EXTRA_STREAM, Uri.parse("content://path/to/email/attachment"))
        }
        startActivity(Intent.createChooser(mailIntent, "Send mail..."))
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            super.onBackPressed()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        }
        return super.onOptionsItemSelected(item)
    }
}