package com.koroyan.smarthome.ui.main_dashboard.more


import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.koroyan.smarthome.App
import com.koroyan.smarthome.R
import com.koroyan.smarthome.data.preferencedata.SharedPreferences
import com.koroyan.smarthome.network.firebase.Firebase
import com.koroyan.smarthome.network.firebase.OnReadListener
import com.koroyan.smarthome.ui.authentification.LoginActivity
import com.koroyan.smarthome.ui.main_dashboard.more.contactus.ContactUsActivity
import com.koroyan.smarthome.ui.main_dashboard.more.api.ApiActivity
import com.koroyan.smarthome.ui.main_dashboard.more.editRooms.EditRoomsActivity
import com.koroyan.smarthome.ui.main_dashboard.more.help.HelpActivity
import com.koroyan.smarthome.ui.main_dashboard.more.myprofile.MyProfileActivity
import com.koroyan.smarthome.ui.main_dashboard.more.myprofile.User
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.fragment_more.view.*


class MoreFragment : Fragment() {

    private var itemView: View? = null
    private lateinit var auth: FirebaseAuth
    private lateinit var user: FirebaseUser


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (itemView == null)
            itemView = inflater.inflate(R.layout.fragment_more, container, false)
        init()
        listeners()
        return itemView
    }

    private fun init() {
        auth = FirebaseAuth.getInstance()
        user = auth.currentUser!!
    }

    override fun onResume() {
        super.onResume()
        Handler().postDelayed({setUser()},2000)
    }

    private fun listeners() {
        itemView!!.viewProfileTextView.setOnClickListener {
            activity?.startActivity(Intent(activity, MyProfileActivity::class.java))
        }

        itemView!!.editRooms.setOnClickListener {
            activity?.startActivity(Intent(activity,EditRoomsActivity::class.java))
        }
        itemView!!.editDevices.setOnClickListener {
            activity?.startActivity(Intent(activity, ApiActivity::class.java))
        }
        itemView!!.help.setOnClickListener {
            activity?.startActivity(Intent(activity, HelpActivity::class.java))
        }
        itemView!!.contactUs.setOnClickListener {
            activity?.startActivity(Intent(activity, ContactUsActivity::class.java))
        }
        itemView!!.logout.setOnClickListener {
            SharedPreferences.removeString(SharedPreferences.MAIL_FOR_FIREBASE)
            auth.signOut()
        }


        auth.addAuthStateListener {
            if (auth.currentUser == null) {
                SharedPreferences.clearAll()
                activity?.startActivity(
                    Intent(
                        activity,
                        LoginActivity::class.java
                    ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                )
            }
        }
    }

    private fun setUser(){
        Firebase.getUser(object :OnReadListener{
            override fun readUser(user: User) {
                itemView!!.findViewById<TextView>(R.id.userFullNameTextView).text = user.name
                    Glide.with(App.instance.context!!).load(user.profileImage).placeholder(R.mipmap.ic_avatar_placeholder)
                        .into(itemView!!.findViewById<CircleImageView>(R.id.profileImageView))
            }
        })
    }
}