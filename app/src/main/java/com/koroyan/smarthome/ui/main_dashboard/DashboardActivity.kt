package com.koroyan.smarthome.ui.main_dashboard

import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.koroyan.smarthome.MyReceiver
import com.koroyan.smarthome.R
import com.koroyan.smarthome.data.preferencedata.SharedPreferences
import com.koroyan.smarthome.network.firebase.Firebase
import com.koroyan.smarthome.ui.main_dashboard.more.MoreFragment
import com.koroyan.smarthome.ui.main_dashboard.rooms.RoomsFragment
import com.koroyan.smarthome.ui.main_dashboard.adapters.ViewPagerAdapter
import com.koroyan.smarthome.ui.main_dashboard.music.PowerFragment
import com.koroyan.smarthome.ui.main_dashboard.warnings.WarningsFragment
import kotlinx.android.synthetic.main.activity_dashboard.*

class DashboardActivity : AppCompatActivity() {

    private val myReceiver by lazy {
        MyReceiver()
    }

    private val fragments = arrayListOf<Fragment>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        init()
        viewPagerListener()
        navViewListener()
    }

    private fun init() {
        fragments.add(RoomsFragment())
        fragments.add(WarningsFragment())
        fragments.add(PowerFragment())
        fragments.add(MoreFragment())
        viewPager.adapter =
            ViewPagerAdapter(
                supportFragmentManager,
                fragments
            )
        viewPager.scrollBarSize = 4
        Firebase.setToken(SharedPreferences.getString(SharedPreferences.TOKEN)!!)
    }

    override fun onResume() {
        super.onResume()
        val intentFilter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        registerReceiver(myReceiver, intentFilter)
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(myReceiver)
    }

    private fun viewPagerListener() {
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                navView.menu.getItem(position).isChecked = true
            }
        })
    }

    private fun navViewListener() {

        navView.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_room -> viewPager.currentItem = 0
                R.id.navigation_flammables -> viewPager.currentItem = 1
                R.id.navigation_music -> viewPager.currentItem = 2
                R.id.navigation_more -> viewPager.currentItem = 3
            }
            true
        }
    }
}
