package com.koroyan.smarthome.ui.main_dashboard.more.myprofile


import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.koroyan.smarthome.R
import com.koroyan.smarthome.extensions.isEmailValid
import com.koroyan.smarthome.tools.Tools
import com.koroyan.smarthome.callbacks.OnStatusListener
import com.koroyan.smarthome.data.preferencedata.SharedPreferences
import com.koroyan.smarthome.extensions.setToolBar
import com.koroyan.smarthome.image_chooser.EasyImage
import com.koroyan.smarthome.image_chooser.ImageChooserUtils
import com.koroyan.smarthome.image_chooser.MediaFile
import com.koroyan.smarthome.image_chooser.MediaSource
import com.koroyan.smarthome.network.firebase.Firebase
import com.koroyan.smarthome.ui.main_dashboard.more.myprofile.User
import kotlinx.android.synthetic.main.activity_my_profile.*
import kotlinx.android.synthetic.main.activity_my_profile.emailAddressEditText
import kotlinx.android.synthetic.main.activity_my_profile.fullNameEditText
import kotlinx.android.synthetic.main.activity_my_profile.imageButtonNext
import kotlinx.android.synthetic.main.layout_toolbar.*

class MyProfileActivity : AppCompatActivity(), View.OnClickListener,
    OnStatusListener {

    private var myProfileViewModel: MyProfileViewModel? = null
    private var filePath: Uri = Uri.EMPTY

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_profile)
        init()
    }

    private fun init() {
        setToolBar(getString(R.string.my_profile), true)

        myProfileViewModel = ViewModelProvider(this)[MyProfileViewModel::class.java]
        myProfileViewModel?.setStatusListener(this)
        emailAddressEditText.isEmailValid(this)
        icCamera.setOnClickListener(this)
        imageButtonNext.setOnClickListener(this)
        getData()
        //myProfileViewModel!!.getUser()
    }

    private fun getData() {
        myProfileViewModel!!.user.observe(this, Observer {
            setData(it)
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            super.onBackPressed()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        }
        return super.onOptionsItemSelected(item)
    }

    private fun updateProfile() {
        val fullName = fullNameEditText.text.toString()
        val emailAddress = emailAddressEditText.text.toString()
        val phoneNumber = phoneNumberEditText.text.toString()

        myProfileViewModel!!.setData(SharedPreferences.getString(SharedPreferences.MAIL_FOR_FIREBASE)!!,filePath, fullName, emailAddress, phoneNumber)
    }

    private fun setData(user: User) {
        fullNameEditText.text = Editable.Factory().newEditable(user.name)
        emailAddressEditText.text = Editable.Factory().newEditable(user.email)
        phoneNumberEditText.text = Editable.Factory().newEditable(user.phone)
        Glide.with(this).load(user.profileImage).placeholder(R.mipmap.ic_avatar_placeholder)
            .into(profilePic)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.icCamera -> choosePhoto()
            R.id.imageButtonNext -> updateProfile()
        }
    }

    override fun onSuccess() {
        progressBar.visibility = GONE
       // myProfileViewModel!!.getUser()
        super.onBackPressed()
    }

    override fun onProgress() {
        progressBar.visibility = VISIBLE
    }

    override fun onFailed(error: String) {
        progressBar.visibility = GONE
        Tools.errorDialog(this@MyProfileActivity, "Incorrect Request", error)
    }


    private fun choosePhoto() {
        if (ImageChooserUtils.hasReadExternalStoragePermission() && ImageChooserUtils.hasWriteExternalStoragePermission() && ImageChooserUtils.hasCameraPermission()) {
            ImageChooserUtils.chooseResource(this)
        } else {
            ImageChooserUtils.requestPermission(this)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == ImageChooserUtils.PERMISSIONS_REQUEST) {
            if (grantResults.size > 2) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    ImageChooserUtils.chooseResource(this)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        ImageChooserUtils.easyImage.handleActivityResult(
            requestCode,
            resultCode,
            data,
            this,
            object : EasyImage.Callbacks {
                override fun onMediaFilesPicked(
                    imageFiles: Array<MediaFile>,
                    source: MediaSource
                ) {
                    if (imageFiles.isNotEmpty()) {
                        Glide.with(applicationContext).load(imageFiles[0].uri).into(profilePic)
                        filePath = imageFiles[0].uri
                    }
                }

                override fun onImagePickerError(
                    error: Throwable,
                    source: MediaSource
                ) {
                }

                override fun onCanceled(source: MediaSource) {
                }
            })
    }
}