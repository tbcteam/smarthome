package com.koroyan.smarthome.ui.main_dashboard.rooms.adapters.rooms

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.koroyan.smarthome.R
import com.koroyan.smarthome.extensions.checked
import com.koroyan.smarthome.ui.main_dashboard.rooms.models.Room
import kotlinx.android.synthetic.main.room_recyclerview_item.view.*

class RoomRecyclerView(private val onCheckListener: OnCheckListener,private val onItemClickListener: OnItemClickListener) : RecyclerView.Adapter<RoomRecyclerView.ViewHolder>() {

    private var rooms:MutableList<Room> = mutableListOf()
    private lateinit var model:Room
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),View.OnClickListener {

        fun onBind(){
            model = rooms[adapterPosition]
            with(itemView){
                roomTitleTextView.text = model.title
                roomTemperatureTextView.text = model.temperature.toString()
                Glide.with(this).load(model.background).placeholder(R.drawable.ic_category_card_view_default).into(cardBackgroundImageView)
                cameraFrameButton.checked = model.cameraDevice
                unitFrameButton.checked = model.unitDevice
                musicFrameButton.checked = model.musicDevice
                lampFrameButton.checked = model.lampDevice
            }
            itemView.cameraFrameButton.setOnClickListener(this)
            itemView.unitFrameButton.setOnClickListener(this)
            itemView.musicFrameButton.setOnClickListener(this)
            itemView.lampFrameButton.setOnClickListener(this)
            itemView.setOnClickListener { onItemClickListener.onItemClick(it,model) }
        }

        override fun onClick(v: View?) {
            onCheckListener.onCheck(v!!,rooms[adapterPosition])
            notifyItemChanged(adapterPosition)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.room_recyclerview_item, parent, false)
        )
    }

    override fun getItemCount(): Int = rooms.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       holder.onBind()
    }

    fun setData(rooms:MutableList<Room>){
        this.rooms = rooms
        notifyDataSetChanged()
    }
}