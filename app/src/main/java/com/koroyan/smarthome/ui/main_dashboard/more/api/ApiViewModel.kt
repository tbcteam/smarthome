package com.koroyan.smarthome.ui.main_dashboard.more.api

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.koroyan.smarthome.data.preferencedata.SharedPreferences
import com.koroyan.smarthome.ui.main_dashboard.more.api.models.ApiMap

class ApiViewModel:ViewModel() {
    private val host = "https://smart-home-25190.firebaseio.com/"
    private val id = SharedPreferences.getString(SharedPreferences.MAIL_FOR_FIREBASE).toString()
    private val _api = MutableLiveData<MutableList<ApiMap>>().apply {
        value = mutableListOf(
            ApiMap("firebase host:", host),
            ApiMap("firebase auth:","iuFZi8DZdVwvrSumhFReyetFFEVn1QYcjBSKeYOU"),
            ApiMap("FCM token:",SharedPreferences.getString(SharedPreferences.TOKEN)!!),
            ApiMap("GET FCM token:","tokens/${id}/token"),
            ApiMap("FCM Server:","AAAAb_mB27s:APA91bGxl8XhCD7RI5qGYn6F11o6Mudv980-lfUg6pWvQht-k2LcC2dj7bfyGveSc1k2RDyqe-5QyHsfdLOuf1RgjRobWfhyeq5TgzT0KbUUOQi97nJaqDTmItRjuGNemZagAesyoxLQ"),
            ApiMap("userId:",id),
            ApiMap("GET user:","users/${id}/user"),
            ApiMap("room model keys ","title:string, temperature:String, background:String, liveCameraUrl:String  cameraDevice:Boolean, lampDevice:Boolean, musicDevice:Boolean, unitDevice:Boolean,  "),
            ApiMap("GET rooms array:","rooms/${id}/category/rooms"),
            ApiMap("Get Room:","rooms/${id}/category/rooms/roomName"),
            ApiMap("POST set temperature:","rooms/${id}/category/rooms/roomMame/temperature"),
            ApiMap("POST select device:","rooms/${id}/category/rooms/roomMame/deviceName"),
            ApiMap("POST add flammable:","${id}/warnings/name")
        )

    }
    val api: LiveData<MutableList<ApiMap>> = _api
}