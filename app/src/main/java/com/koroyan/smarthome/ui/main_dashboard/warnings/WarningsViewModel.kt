package com.koroyan.smarthome.ui.main_dashboard.warnings

import android.util.Log.d
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.koroyan.smarthome.network.firebase.Firebase
import com.koroyan.smarthome.network.firebase.OnReadListener
import com.koroyan.smarthome.ui.main_dashboard.warnings.models.WarningModel

class WarningsViewModel : ViewModel() {

    private val _warnings = MutableLiveData<WarningModel>().apply {
        Firebase.readWarnings(object :OnReadListener{
            override fun warningsRead(flammables: WarningModel) {
                value = flammables
            }
        })
    }
    val warnings: LiveData<WarningModel> = _warnings

    fun removeWarnings(){
        Firebase.removeWarnings()
    }
}