package com.koroyan.smarthome.ui.main_dashboard.music.band

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.koroyan.smarthome.R
import com.koroyan.smarthome.extensions.setGradientColor
import kotlinx.android.synthetic.main.bands_recyclerview_layout.view.*


class BandsRecyclerViewAdapter(
    private val bands: MutableList<BandModel>,
    private val customListener: CustomListener
) : RecyclerView.Adapter<BandsRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.bands_recyclerview_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount(): Int {
        return bands.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var band: BandModel

        fun onBind() {
            band = bands[adapterPosition]

            itemView.apply {
                setOnClickListener {
                    customListener.onClick(adapterPosition)
                }

                Glide.with(this.context)
                    .load(band.imgUrl)
                    .placeholder(R.mipmap.placeholder)
                    .into(this.bandImageView)

                bandNameTextView.text = band.name
                bandNameTextView.setGradientColor()
                bandGenreTextView.text = band.genre
            }
        }
    }
}