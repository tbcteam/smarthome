package com.koroyan.smarthome.ui.main_dashboard.more.help

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import com.koroyan.smarthome.R
import com.koroyan.smarthome.extensions.setToolBar
import com.koroyan.smarthome.ui.main_dashboard.more.help.adapters.HelpRecyclerViewAdapter
import kotlinx.android.synthetic.main.activity_help.*
import kotlinx.android.synthetic.main.layout_toolbar.*

class HelpActivity : AppCompatActivity() {

    private lateinit var adapter: HelpRecyclerViewAdapter
    private val helpTexts = mutableListOf<HelpTextsModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_help)
        init()
    }

    private fun init() {
        setToolBar(getString(R.string.help), true)

        helpTexts.add(
            HelpTextsModel(
                "Device is not connecting",
                loremIpsumText
            )
        )
        helpTexts.add(
            HelpTextsModel(
                "Can't add new room",
                loremIpsumText
            )
        )
        helpTexts.add(
            HelpTextsModel(
                "Ac is not connected",
                loremIpsumText
            )
        )
        helpTexts.add(
            HelpTextsModel(
                "How to change my picture",
                loremIpsumText
            )
        )
        helpTexts.add(
            HelpTextsModel(
                "Change room background",
                loremIpsumText
            )
        )
        helpTexts.add(
            HelpTextsModel(
                "Connection problem",
                loremIpsumText
            )
        )
        helpTexts.add(
            HelpTextsModel(
                "Deactivate account",
                loremIpsumText
            )
        )

        helpRecyclerView.layoutManager = LinearLayoutManager(this)
        adapter = HelpRecyclerViewAdapter(
            helpTexts
        )
        helpRecyclerView.adapter = adapter

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            super.onBackPressed()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        }
        return super.onOptionsItemSelected(item)
    }

    private val loremIpsumText =
        "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book."

}