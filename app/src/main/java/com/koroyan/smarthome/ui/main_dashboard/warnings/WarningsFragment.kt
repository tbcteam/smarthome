package com.koroyan.smarthome.ui.main_dashboard.warnings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.koroyan.smarthome.R
import com.koroyan.smarthome.ui.main_dashboard.warnings.adapters.WarningsMessagesAdapter
import com.koroyan.smarthome.ui.main_dashboard.warnings.models.WarningModel

class WarningsFragment : Fragment() {

    private lateinit var warningsMessageAdapter: WarningsMessagesAdapter
    private lateinit var warningsLayout: LinearLayoutManager
    private lateinit var warnings: MutableList<WarningModel>

    private lateinit var warningsViewModel: WarningsViewModel
    private var root: View? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        warningsViewModel =
            ViewModelProvider(this)[WarningsViewModel::class.java]
        if (root == null)
            root = inflater.inflate(R.layout.fragment_warnings, container, false)

        init()
        getData()
        return root
    }

    private fun init() {
        warnings = mutableListOf()
        warningsLayout = LinearLayoutManager(activity)
        warningsMessageAdapter = WarningsMessagesAdapter()

        root!!.findViewById<RecyclerView>(R.id.recyclerView).apply {
            layoutManager = warningsLayout
            adapter = warningsMessageAdapter
        }

        root!!.findViewById<Button>(R.id.okButton).setOnClickListener {
            warningsViewModel.removeWarnings()
            warnings.clear()
            warningsMessageAdapter.setData(warnings)
            root!!.findViewById<ImageView>(R.id.allOkImageView).visibility = VISIBLE
            root!!.findViewById<Button>(R.id.okButton).visibility = GONE
        }
    }

    private fun getData() {
        warningsViewModel.warnings.observe(viewLifecycleOwner, Observer {
            warningsMessageAdapter.setData(mutableListOf())
            warnings.add(it)
            if (warnings.isNotEmpty()) {
                root!!.findViewById<Button>(R.id.okButton).visibility = VISIBLE
                root!!.findViewById<ImageView>(R.id.allOkImageView).visibility = GONE
            }
            warningsMessageAdapter.setData(warnings)
        })
    }
}