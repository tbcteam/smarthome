package com.koroyan.smarthome.ui.main_dashboard.music.band

interface CustomListener {
    fun onClick(position: Int)
}