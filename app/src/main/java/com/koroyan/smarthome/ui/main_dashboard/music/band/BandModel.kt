package com.koroyan.smarthome.ui.main_dashboard.music.band

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class BandModel() : Parcelable {
    var name = ""

    @SerializedName("img_url")
    var imgUrl = ""
    var info = ""
    var genre = ""
    var songs = mutableListOf<SongModel>()

    class SongModel {
        var title = ""
    }

    constructor(parcel: Parcel) : this() {
        name = parcel.readString()!!
        imgUrl = parcel.readString()!!
        info = parcel.readString()!!
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(imgUrl)
        parcel.writeString(info)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<BandModel> {
        override fun createFromParcel(parcel: Parcel): BandModel {
            return BandModel(
                parcel
            )
        }

        override fun newArray(size: Int): Array<BandModel?> {
            return arrayOfNulls(size)
        }
    }
}