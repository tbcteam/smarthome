package com.koroyan.smarthome.ui.main_dashboard.rooms.livecamera


import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.VideoView
import androidx.appcompat.app.AppCompatActivity
import com.koroyan.smarthome.R


class LiveCameraActivity : AppCompatActivity() {
    var videoView: VideoView? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_live_camera)
        start()
    }

    private fun start() {
        //val url = intent.getStringExtra(IntentKeys.LIVE_CAMERA)
        val httpLiveUrl =
            "https://5b44cf20b0388.streamlock.net:8443/vod/smil:bbb.smil/playlist.m3u8"
        videoView = findViewById<View>(R.id.VideoView) as VideoView
        videoView!!.setVideoURI(Uri.parse(httpLiveUrl))
        videoView!!.setMediaController(android.widget.MediaController(this))
        videoView!!.requestFocus()
        videoView!!.start()
    }
}