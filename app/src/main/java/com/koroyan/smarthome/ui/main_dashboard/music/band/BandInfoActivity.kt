package com.koroyan.smarthome.ui.main_dashboard.music.band

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.koroyan.smarthome.R
import com.koroyan.smarthome.extensions.setToolBar
import com.koroyan.smarthome.network.CustomCallback
import com.koroyan.smarthome.network.Endpoints
import com.koroyan.smarthome.network.HttpRequest
import com.koroyan.smarthome.ui.main_dashboard.music.song.SongLyricsActivity
import com.koroyan.smarthome.ui.main_dashboard.music.song.SongRecyclerViewAdapter
import kotlinx.android.synthetic.main.activity_band_info.*
import org.json.JSONObject

class BandInfoActivity : AppCompatActivity() {

    private lateinit var songsAdapter: SongRecyclerViewAdapter
    private val songs = mutableListOf<BandModel.SongModel>()
    private val mySongs = mutableListOf<BandModel.SongModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_band_info)
        init()
    }

    private fun init() {
        val band = intent.getParcelableExtra<BandModel>("band")
        val bandName = band!!.name
        val bandInfo = band.info
        val bandImgUrl = band.imgUrl

        setToolBar(bandName, true)

        Glide.with(this)
            .load(bandImgUrl)
            .placeholder(R.mipmap.placeholder)
            .into(bandImageView)
        bandInfoTextView.text = bandInfo

        requestSongTitles(band, bandName)

        searchEditText.addTextChangedListener(textWatcher)

    }

    private fun requestSongTitles(band: BandModel, bandName: String) {
        HttpRequest.getRequest(Endpoints.SONG_TITLES, object : CustomCallback {
            override fun onFailure(error: String) {
                Toast.makeText(this@BandInfoActivity, error, Toast.LENGTH_LONG).show()
            }

            override fun onResponse(response: String) {
                val jsonArray = JSONObject(response).getJSONArray("data")

                (0 until jsonArray.length()).forEach {
                    val jsonBand = jsonArray.get(it) as JSONObject

                    if (jsonBand.getString("band") == bandName) {
                        val jsonSongs = jsonBand.getJSONArray("songs")

                        if (jsonSongs.length() > 0) {
                            (0 until jsonSongs.length()).forEach {
                                val jsonSong = jsonSongs.get(it)
                                val song = Gson().fromJson(
                                    jsonSong.toString(),
                                    BandModel.SongModel::class.java
                                )
                                band.songs.add(song)
                            }
                        }
                    }
                }
                songs.addAll(band.songs)
                mySongs.addAll(band.songs)

                if (songs.isEmpty()) {
                    noSongWasFoundTextView.visibility = View.VISIBLE
                } else {
                    noSongWasFoundTextView.visibility = View.GONE
                }

                recyclerView.layoutManager = LinearLayoutManager(this@BandInfoActivity)
                songsAdapter =
                    SongRecyclerViewAdapter(
                        songs,
                        object :
                            CustomListener {
                            override fun onClick(position: Int) {
                                val intent =
                                    Intent(
                                        this@BandInfoActivity,
                                        SongLyricsActivity::class.java
                                    ).apply {
                                        putExtra("artist", bandName)
                                        putExtra("title", band.songs[position].title)
                                    }
                                startActivity(intent)
                            }
                        })
                recyclerView.adapter = songsAdapter
            }
        })
    }


    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {

        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            getSongs(s.toString())
        }
    }

    private fun getSongs(input: String) {
        songs.clear()
        songsAdapter.notifyDataSetChanged()

        if (input.isEmpty()) {
            songs.addAll(mySongs)
            songsAdapter.notifyDataSetChanged()
            return
        }

        for (eachSong in 0 until mySongs.size) {
            if (mySongs[eachSong].title.toLowerCase().contains(input.toLowerCase())) {
                songs.add(mySongs[eachSong])
            }
        }

        if (songs.isEmpty()) {
            noSongWasFoundTextView.visibility = View.VISIBLE
        } else {
            noSongWasFoundTextView.visibility = View.GONE
        }

        songsAdapter.notifyDataSetChanged()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            super.onBackPressed()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        }
        return super.onOptionsItemSelected(item)
    }

}