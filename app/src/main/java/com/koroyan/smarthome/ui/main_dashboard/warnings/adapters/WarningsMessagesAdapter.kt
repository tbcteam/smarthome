package com.koroyan.smarthome.ui.main_dashboard.warnings.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.koroyan.smarthome.R
import com.koroyan.smarthome.ui.main_dashboard.warnings.models.WarningModel
import kotlinx.android.synthetic.main.flammable_recyclerview_item.view.*

class WarningsMessagesAdapter:RecyclerView.Adapter<WarningsMessagesAdapter.ViewHolder>() {
    private var flammables:MutableList<WarningModel> = mutableListOf()
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        lateinit var model:WarningModel
        fun onBind(){
            model = flammables[adapterPosition]
            with(itemView){
                Glide.with(this).load(model.errorIcon).into(flammableIconImageView)
                errorMessageTextView.text = model.errorMessage
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.flammable_recyclerview_item,parent,false))
    }

    override fun getItemCount(): Int = flammables.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       holder.onBind()
    }

    fun setData(flammables:MutableList<WarningModel>){
        this.flammables = flammables
        notifyDataSetChanged()
    }
}