package com.koroyan.smarthome.ui.main_dashboard.rooms

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.Glide
import com.koroyan.smarthome.R
import com.koroyan.smarthome.callbacks.OnStatusListener
import com.koroyan.smarthome.image_chooser.EasyImage
import com.koroyan.smarthome.image_chooser.ImageChooserUtils
import com.koroyan.smarthome.image_chooser.MediaFile
import com.koroyan.smarthome.image_chooser.MediaSource
import com.koroyan.smarthome.ui.main_dashboard.rooms.addrooms.AddRoomsActivity
import com.koroyan.smarthome.ui.main_dashboard.rooms.livecamera.LiveCameraActivity
import com.koroyan.smarthome.ui.main_dashboard.rooms.adapters.categories.CategoryRecyclerViewAdapter
import com.koroyan.smarthome.ui.main_dashboard.rooms.adapters.categories.OnItemClickListener
import com.koroyan.smarthome.ui.main_dashboard.rooms.adapters.rooms.OnCheckListener
import com.koroyan.smarthome.ui.main_dashboard.rooms.adapters.rooms.RoomRecyclerView
import com.koroyan.smarthome.ui.main_dashboard.rooms.models.Category
import com.koroyan.smarthome.ui.main_dashboard.rooms.models.Room
import com.koroyan.smarthome.utils.IntentKeys
import kotlinx.android.synthetic.main.activity_sign_up.*

class RoomsFragment : Fragment() {

    private lateinit var categoryRecyclerViewAdapter: CategoryRecyclerViewAdapter
    private lateinit var categoryLayout: LinearLayoutManager
    private lateinit var rooms: MutableList<Room>

    private lateinit var roomRecyclerView: RoomRecyclerView
    private lateinit var roomLayout: LinearLayoutManager

    private var addRoom: Button? = null

    private var swipeRefreshLayout: SwipeRefreshLayout? = null

    private var category = "In Home"

    private lateinit var roomsViewModel: RoomsViewModel
    private var root: View? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        roomsViewModel =
            ViewModelProvider(this)[RoomsViewModel::class.java]
        if (root == null)
            root = inflater.inflate(R.layout.fragment_rooms, container, false)
        init()
        return root
    }

    private fun init() {
        initCategoryRecyclerView()
        initRoomRecyclerView()

        rooms = mutableListOf()

        swipeRefreshLayout = root?.findViewById(R.id.swipeRefreshLayout)
        swipeRefreshListener()
        addRoom = root?.findViewById(R.id.addRoom)
        addRoom?.setOnClickListener {
            startActivity(
                Intent(
                    activity,
                    AddRoomsActivity::class.java
                ).putExtra(IntentKeys.CATEGORY, category)
            )
        }
    }


    private fun swipeRefreshListener() {
        swipeRefreshLayout?.setOnRefreshListener {
            swipeRefreshLayout?.isRefreshing = true

            Handler().postDelayed({
                rooms.clear()
                roomRecyclerView.setData(rooms)
                roomsViewModel.readRooms(category)
                swipeRefreshLayout?.isRefreshing = false
            }, 500)
        }
    }

    private fun initCategoryRecyclerView() {
        categoryRecyclerViewAdapter =
            CategoryRecyclerViewAdapter(
                onCategoryItemClickListener
            )
        categoryLayout = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        root!!.findViewById<RecyclerView>(R.id.categoryRecyclerView).apply {
            adapter = categoryRecyclerViewAdapter
            layoutManager = categoryLayout
        }

        roomsViewModel.categories.observe(viewLifecycleOwner, Observer {
            categoryRecyclerViewAdapter.setData(it)
        })
    }

    private fun initRoomRecyclerView() {
        roomRecyclerView = RoomRecyclerView(
            onRoomFrameButtonCheckListener,
            object :
                com.koroyan.smarthome.ui.main_dashboard.rooms.adapters.rooms.OnItemClickListener {
                override fun onItemClick(v: View, room: Room) {
                    val intent = Intent(activity, LiveCameraActivity::class.java)
                    intent.putExtra(IntentKeys.LIVE_CAMERA, room.liveCameraUrl)
                    activity?.startActivity(intent)
                }
            }
        )
        roomLayout = LinearLayoutManager(activity)
        root!!.findViewById<RecyclerView>(R.id.roomRecyclerView).apply {
            adapter = roomRecyclerView
            layoutManager = roomLayout
        }

        roomsViewModel.rooms.observe(viewLifecycleOwner, Observer {
            rooms.clear()
            roomRecyclerView.setData(rooms)
            rooms = it
            roomRecyclerView.setData(rooms)
        })

    }

    private val onCategoryItemClickListener = object :
        OnItemClickListener {
        override fun onItemClick(position: Int, category: Category) {
            this@RoomsFragment.category = category.name
            rooms.clear()
            roomRecyclerView.setData(rooms)
            roomsViewModel.readRooms(category.name)
        }

    }

    private val onRoomFrameButtonCheckListener = object : OnCheckListener {
        override fun onCheck(v: View, room: Room) {
            when (v.id) {
                R.id.cameraFrameButton -> {
                    room.cameraDevice = !room.cameraDevice
                }
                R.id.unitFrameButton -> {
                    room.unitDevice = !room.unitDevice
                }
                R.id.musicFrameButton -> {
                    room.musicDevice = !room.musicDevice
                }
                R.id.lampFrameButton -> {
                    room.lampDevice = !room.lampDevice

                }
            }
            roomsViewModel.checkDevice(category, room, object : OnStatusListener {
                override fun onSuccess() {
                    v.isClickable = true
                    swipeRefreshLayout?.isRefreshing = false
                }

                override fun onProgress() {
                    v.isClickable = false
                    swipeRefreshLayout?.isRefreshing = true
                }

                override fun onFailed(error: String) {
                    v.isClickable = true
                    swipeRefreshLayout?.isRefreshing = false
                }

            })
        }

    }



}