package com.koroyan.smarthome.ui.main_dashboard.more.editRooms.adapters.room

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.koroyan.smarthome.R
import com.koroyan.smarthome.extensions.checked
import com.koroyan.smarthome.ui.main_dashboard.rooms.models.Room
import kotlinx.android.synthetic.main.item_recyclerview_edit_room.view.*
import kotlinx.android.synthetic.main.room_recyclerview_item.view.cameraFrameButton
import kotlinx.android.synthetic.main.room_recyclerview_item.view.cardBackgroundImageView
import kotlinx.android.synthetic.main.room_recyclerview_item.view.lampFrameButton
import kotlinx.android.synthetic.main.room_recyclerview_item.view.musicFrameButton
import kotlinx.android.synthetic.main.room_recyclerview_item.view.roomTitleTextView
import kotlinx.android.synthetic.main.room_recyclerview_item.view.unitFrameButton

class RoomAdapter(private val onItemClickListener: OnItemClickListener):RecyclerView.Adapter<RoomAdapter.ViewHolder>() {

    private var rooms:MutableList<Room> = mutableListOf()

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        lateinit var model:Room
        fun onBind(){
            model = rooms[adapterPosition]
            with(itemView){
                roomTitleTextView.text = model.title
                Glide.with(this).load(model.background).placeholder(R.drawable.ic_category_card_view_default).into(cardBackgroundImageView)
                cameraFrameButton.checked = model.cameraDevice
                unitFrameButton.checked = model.unitDevice
                musicFrameButton.checked = model.musicDevice
                lampFrameButton.checked = model.lampDevice
            }
            itemView.popupMenuImageButton.setOnClickListener{
                onItemClickListener.onItemClick(it,model)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_recyclerview_edit_room,parent,false))
    }

    override fun getItemCount(): Int = rooms.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    fun setData(rooms:MutableList<Room>){
        this.rooms = rooms
        notifyDataSetChanged()
    }
}