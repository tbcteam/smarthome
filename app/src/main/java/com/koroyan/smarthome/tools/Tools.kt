package com.koroyan.smarthome.tools

import android.app.Activity
import android.app.Dialog
import android.util.Patterns
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import com.koroyan.smarthome.R
import kotlinx.android.synthetic.main.error_dialog_layout.*

object Tools {
    fun isEmailValid(text: String) = Patterns.EMAIL_ADDRESS.matcher(text).matches()

    fun errorDialog(context: Activity, title: String, description: String) {
        val dialog = Dialog(context)
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.error_dialog_layout)

        val params: ViewGroup.LayoutParams = dialog.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog.window!!.attributes = params as WindowManager.LayoutParams
        dialog.titleTV.text = title
        dialog.descriptionTV.text = description
        dialog.okButton.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

}