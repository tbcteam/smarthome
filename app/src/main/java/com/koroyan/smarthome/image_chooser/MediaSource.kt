package com.koroyan.smarthome.image_chooser

enum class MediaSource {
    GALLERY, DOCUMENTS, CAMERA_IMAGE, CAMERA_VIDEO, CHOOSER
}