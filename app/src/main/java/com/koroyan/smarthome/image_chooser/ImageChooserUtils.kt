package com.koroyan.smarthome.image_chooser

import android.app.Activity
import android.app.Dialog
import android.content.pm.PackageManager
import android.os.Build
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import androidx.core.app.ActivityCompat
import com.koroyan.smarthome.App
import com.koroyan.smarthome.R
import kotlinx.android.synthetic.main.choose_resource_dialog_layout.*

class ImageChooserUtils {
    companion object {
        const val PERMISSIONS_REQUEST = 22

        fun hasReadExternalStoragePermission() = ActivityCompat.checkSelfPermission(
            App.instance.context!!,
            android.Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED

        fun hasWriteExternalStoragePermission() = ActivityCompat.checkSelfPermission(
            App.instance.context!!,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED

        fun hasCameraPermission() = ActivityCompat.checkSelfPermission(
            App.instance.context!!,
            android.Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED

        fun requestPermission(activity: Activity) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                activity.requestPermissions(
                    arrayOf(
                        android.Manifest.permission.CAMERA,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ), PERMISSIONS_REQUEST
                )
            }
        }

        val easyImage by lazy {
            EasyImage.Builder(App.instance.context!!) // Chooser only
                // Will appear as a system chooser title, DEFAULT empty string
                //.setChooserTitle("Pick media")
                // Will tell chooser that it should show documents or gallery apps
                //.setChooserType(ChooserType.CAMERA_AND_DOCUMENTS)  you can use this or the one below
                .setChooserType(ChooserType.CAMERA_AND_GALLERY)
                // Setting to true will cause taken pictures to show up in the device gallery, DEFAULT false
                .setCopyImagesToPublicGalleryFolder(false) // Sets the name for images stored if setCopyImagesToPublicGalleryFolder = true
                .setFolderName("EasyImage sample") // Allow multiple picking
                .allowMultiple(false)
                .build()
        }

        fun chooseResource(activity: Activity) {
            val dialog = Dialog(activity)
            dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
            dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(R.layout.choose_resource_dialog_layout)

            val params: ViewGroup.LayoutParams = dialog.window!!.attributes
            params.width = ViewGroup.LayoutParams.MATCH_PARENT
            params.height = ViewGroup.LayoutParams.WRAP_CONTENT
            dialog.window!!.attributes = params as WindowManager.LayoutParams

            dialog.chooseFromCamera.setOnClickListener {
                easyImage.openCameraForImage(activity)
                dialog.dismiss()
            }

            dialog.ChooseFromGallery.setOnClickListener {
                easyImage.openGallery(activity)
                dialog.dismiss()
            }

            dialog.show()
        }

    }
}