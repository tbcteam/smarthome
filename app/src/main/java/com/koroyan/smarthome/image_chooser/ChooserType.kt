package com.koroyan.smarthome.image_chooser

enum class ChooserType {
    CAMERA_AND_GALLERY, CAMERA_AND_DOCUMENTS
}