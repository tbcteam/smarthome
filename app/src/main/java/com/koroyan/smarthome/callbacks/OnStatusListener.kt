package com.koroyan.smarthome.callbacks

interface OnStatusListener {
    fun onSuccess()
    fun onProgress()
    fun onFailed(error:String)
}