package com.koroyan.smarthome

import android.app.Application
import android.content.Context

class App:Application() {
    companion object{
        val instance by lazy {
            this
        }
        var context:Context? = null
    }

    override fun onCreate() {
        super.onCreate()
        context = applicationContext
    }

    fun getContext() = context
}