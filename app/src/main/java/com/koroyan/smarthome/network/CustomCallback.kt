package com.koroyan.smarthome.network

interface CustomCallback {
    fun onFailure(error: String)
    fun onResponse(response: String)
}