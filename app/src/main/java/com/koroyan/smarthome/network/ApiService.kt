package com.koroyan.smarthome.network

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {
    @GET("{path}")
    fun getRequest(@Path("path") path: String): Call<String>

    @GET("{artist}/{title}")
    fun getRequest(@Path("artist") artist: String, @Path("title") title: String): Call<String>
}