package com.koroyan.smarthome.network.firebase

class MyFirebaseException(error:String):Throwable(error) {
    override val message ="MyFirebaseException: ${super.message.toString()}"
}