package com.koroyan.smarthome.network

import com.koroyan.smarthome.App
import com.koroyan.smarthome.R
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory

object HttpRequest {

    private const val HTTP_200_OK = 200
    private const val HTTP_201_CREATED = 201
    private const val HTTP_204_NO_CONTENT = 204
    private const val HTTP_400_BAD_REQUEST = 400
    private const val HTTP_401_UNAUTHORIZED = 401
    private const val HTTP_404_NOT_FOUND = 404
    private const val HTTP_500_INTERNAL_SERVER_ERROR = 500

    var retrofitForBands = Retrofit.Builder()
        .baseUrl(App.context!!.getString(R.string.domainOfBands))
        .addConverterFactory(ScalarsConverterFactory.create())
        .build()

    var retrofitForLyrics = Retrofit.Builder()
        .baseUrl(App.context!!.getString(R.string.domainOfLyrics))
        .addConverterFactory(ScalarsConverterFactory.create())
        .build()

    var serviceForBands = retrofitForBands.create(
        ApiService::class.java
    )

    var serviceForLyrics = retrofitForLyrics.create(ApiService::class.java)

    fun getRequest(path: String, callback: CustomCallback) {
        val call = serviceForBands.getRequest(path)
        call.enqueue(onCallback(callback))
    }

    fun getRequest(artist: String, title: String, callback: CustomCallback) {
        val call = serviceForLyrics.getRequest(artist, title)
        call.enqueue(onCallback(callback))
    }

    private fun onCallback(callback: CustomCallback) = object : Callback<String> {
        override fun onFailure(call: Call<String>, t: Throwable) {
            callback.onFailure(t.message.toString())
        }

        override fun onResponse(call: Call<String>, response: Response<String>) {
            val statusCode = response.code()

            if (statusCode == HTTP_200_OK || statusCode == HTTP_201_CREATED) {
                callback.onResponse(response.body().toString())
            } else {
                try {
                    val errorJson = JSONObject(response.errorBody()!!.string())
                    val errorString: String?

                    if (errorJson.has("error")) {
                        errorString = errorJson.getString("error")

                        if (statusCode == HTTP_400_BAD_REQUEST ||
                            statusCode == HTTP_401_UNAUTHORIZED ||
                            statusCode == HTTP_404_NOT_FOUND ||
                            statusCode == HTTP_500_INTERNAL_SERVER_ERROR ||
                            statusCode == HTTP_204_NO_CONTENT
                        ) {
                            callback.onFailure(errorString)
                        }
                    }
                } catch (e: JSONException) {
                    callback.onFailure(e.message.toString())
                }
            }
        }

    }


}