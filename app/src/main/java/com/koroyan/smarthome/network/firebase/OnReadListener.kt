package com.koroyan.smarthome.network.firebase

import com.koroyan.smarthome.ui.main_dashboard.more.myprofile.User
import com.koroyan.smarthome.ui.main_dashboard.warnings.models.WarningModel
import com.koroyan.smarthome.ui.main_dashboard.rooms.models.Room

interface OnReadListener {
    fun roomRead(rooms:MutableList<Room>){}
    fun readUser(user: User){}
    fun warningsRead(flammables:WarningModel){}
    fun changed(){}

}