package com.koroyan.smarthome.network.firebase

import android.net.Uri
import android.util.Log.d
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.koroyan.smarthome.callbacks.OnStatusListener
import com.koroyan.smarthome.data.preferencedata.SharedPreferences
import com.koroyan.smarthome.tools.Tools
import com.koroyan.smarthome.ui.main_dashboard.more.contactus.model.Message
import com.koroyan.smarthome.ui.main_dashboard.warnings.models.WarningModel
import com.koroyan.smarthome.ui.main_dashboard.more.myprofile.User
import com.koroyan.smarthome.ui.main_dashboard.rooms.models.Room

object Firebase {


    private val myRef by lazy {
        FirebaseDatabase.getInstance().getReference("users")
    }
    private val myRoomRef by lazy {
        FirebaseDatabase.getInstance().getReference("rooms")
    }
    private val ref by lazy {
        FirebaseDatabase.getInstance().reference
    }
    private val mStorageRef by lazy {
        FirebaseStorage.getInstance().reference
    }
    val mUser by lazy {
        FirebaseAuth.getInstance().currentUser
    }

    private var onStatusListener: OnStatusListener? = null
    fun setStatusListener(onStatusListener: OnStatusListener) {
        this.onStatusListener = onStatusListener
    }

    fun setData(
        id: String,
        image: Uri?,
        name: String,
        email: String,
        phone: String,
        onStatusListener: OnStatusListener?
    ) {
        this.onStatusListener = onStatusListener
        this.onStatusListener?.onProgress()
        if (name.isEmpty()) {
            this.onStatusListener!!.onFailed("name is empty")
            return
        }

        if (!Tools.isEmailValid(email)) {
            this.onStatusListener!!.onFailed("incorrect email address")
            return
        }


        if (image.toString().isNotEmpty()) {
            upload(
                id,
                image,
                name,
                email,
                phone
            )
        } else {
            upDateProfile(
                id,
                image!!,
                name,
                email,
                phone
            )
//            getUser(object : OnReadListener {
//                override fun readUser(user: User) {
//                    upDateProfile(
//                        id,
//                        Uri.parse(user.profileImage),
//                        name,
//                        email,
//                        phone
//                    )
//                }
//            })

        }
    }


    private fun upDateProfile(id: String, photo: Uri, name: String, email: String, phone: String) {
        val profileUpdates =
            UserProfileChangeRequest.Builder()
                .setPhotoUri(photo)
                .setDisplayName(name).build()
        mUser!!.updateProfile(profileUpdates)
        myRef.child("${id}/user")
            .setValue(
                User(
                    name,
                    email,
                    phone,
                    photo.toString()
                )
            )
        this.onStatusListener?.onSuccess()
    }

    private fun upload(id: String, image: Uri?, name: String, email: String, phone: String) {
        val roomRef: StorageReference =
            mStorageRef.child("images/${id}/profile/profile.jpg")
        roomRef.putFile(image!!)
            .addOnSuccessListener {
                roomRef.downloadUrl.addOnSuccessListener {
                    upDateProfile(
                        id,
                        it,
                        name,
                        email,
                        phone
                    )
                    mUser!!.updateProfile(
                        UserProfileChangeRequest.Builder().setDisplayName(name).setPhotoUri(it)
                            .build()
                    )
                    this.onStatusListener?.onSuccess()
                }
            }
            .addOnFailureListener {
                this.onStatusListener?.onFailed(it.toString())
            }
    }


    fun removeRoom(roomCategory: String, room: String) {
        val category = roomCategory.replace("\\s".toRegex(), "")
        val name = room.replace("\\s".toRegex(), "")
        myRoomRef.child("${SharedPreferences.getString(SharedPreferences.MAIL_FOR_FIREBASE)}/${category}/rooms/${name}")
            .removeValue()
    }

    fun changePost(
        oldRoomName: String,
        category: String,
        room: Room,
        onStatusListener: OnStatusListener?
    ) {
        this.onStatusListener = onStatusListener
        this.onStatusListener?.onProgress()
        if (room.background.isNotEmpty()) {
            val roomRef: StorageReference =
                mStorageRef.child("images/${SharedPreferences.getString(SharedPreferences.MAIL_FOR_FIREBASE)}/${category}/${room.title}/room.jpg")
            roomRef.putFile(Uri.parse(room.background))
                .addOnSuccessListener {
                    roomRef.downloadUrl.addOnSuccessListener {
                        changeRoom(it.toString(), category, room.title, oldRoomName)
                    }
                }
                .addOnFailureListener {
                    this.onStatusListener?.onFailed(it.toString())
                }
        } else {
            changeRoom("empty", category, room.title, oldRoomName)
        }
    }

    private fun changeRoom(
        imageUrl: String,
        roomCategory: String,
        roomName: String,
        oldRoomName: String
    ) {
        val category = roomCategory.replace("\\s".toRegex(), "")
        val name = roomName.replace("\\s".toRegex(), "")
        removeRoom(category, oldRoomName)
        myRoomRef.child("${SharedPreferences.getString(SharedPreferences.MAIL_FOR_FIREBASE)}/${category}/rooms/${name}")
            .setValue(Room(roomName, 0.0f, imageUrl))
        this.onStatusListener?.onSuccess()
    }


    fun uploadPost(
        filePath: Uri?,
        category: String,
        roomName: String,
        onStatusListener: OnStatusListener?
    ) {
        this.onStatusListener = onStatusListener
        onStatusListener?.onProgress()
        if (roomName.isEmpty()) {
            onStatusListener!!.onFailed("room name is empty")
            return
        }
        if (filePath != null) {
            val roomRef: StorageReference =
                mStorageRef.child("images/${SharedPreferences.getString(SharedPreferences.MAIL_FOR_FIREBASE)}/${category}/${roomName}/room.jpg")
            roomRef.putFile(filePath)
                .addOnSuccessListener {
                    roomRef.downloadUrl.addOnSuccessListener {
                        addRoom(it.toString(), category, roomName)
                    }
                }
                .addOnFailureListener {
                    onStatusListener?.onFailed(it.toString())
                }
        } else {
            addRoom("empty", category, roomName)
        }
    }

    private fun addRoom(imageUrl: String, roomCategory: String, roomName: String) {
        val category = roomCategory.replace("\\s".toRegex(), "")
        val name = roomName.replace("\\s".toRegex(), "")

        myRoomRef.child("${SharedPreferences.getString(SharedPreferences.MAIL_FOR_FIREBASE)}/${category}/rooms/${name}")
            .setValue(Room(roomName, 0.0f, imageUrl))
        onStatusListener?.onSuccess()
    }

    fun readRooms(roomCategory: String, onReadListener: OnReadListener) {
        val category = roomCategory.replace("\\s".toRegex(), "")
        myRoomRef.child("${SharedPreferences.getString(SharedPreferences.MAIL_FOR_FIREBASE)}/$category")
            .addChildEventListener(object : ChildEventListener {
                override fun onCancelled(p0: DatabaseError) {

                }

                override fun onChildMoved(p0: DataSnapshot, p1: String?) {

                }

                override fun onChildChanged(p0: DataSnapshot, p1: String?) {
                }

                override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                    val rooms = mutableListOf<Room>()
                    p0.children.forEach {
                        rooms.add(it.getValue(Room::class.java)!!)
                    }
                    onReadListener.roomRead(rooms)
                }

                override fun onChildRemoved(p0: DataSnapshot) {

                }

            })
    }

    fun readWarnings(onReadListener: OnReadListener) {
        ref.child("${SharedPreferences.getString(SharedPreferences.MAIL_FOR_FIREBASE)}/warnings")
            .addChildEventListener(object : ChildEventListener {
                override fun onCancelled(p0: DatabaseError) {

                }

                override fun onChildMoved(p0: DataSnapshot, p1: String?) {

                }

                override fun onChildChanged(p0: DataSnapshot, p1: String?) {

                }

                override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                    onReadListener.warningsRead(p0.getValue(WarningModel::class.java)!!)
                }

                override fun onChildRemoved(p0: DataSnapshot) {

                }

            })
    }

    fun removeWarnings() {
        ref.child("${SharedPreferences.getString(SharedPreferences.MAIL_FOR_FIREBASE)}/warnings")
            .removeValue()
    }

    fun checkDevice(roomCategory: String, room: Room, onStatusListener: OnStatusListener?) {
        this.onStatusListener = onStatusListener
        onStatusListener?.onProgress()
        val category = roomCategory.replace("\\s".toRegex(), "")
        val name = room.title.replace("\\s".toRegex(), "")
        myRoomRef.child("${SharedPreferences.getString(SharedPreferences.MAIL_FOR_FIREBASE)}/${category}/rooms/${name}/")
            .setValue(room)
        onStatusListener?.onSuccess()
    }

    fun setToken(token: String) {
        ref.child("tokens/${SharedPreferences.getString(SharedPreferences.MAIL_FOR_FIREBASE)}/token")
            .setValue(token)
        SharedPreferences.saveString(SharedPreferences.TOKEN, token)
    }

    fun sendMessageToAdmin(message: Message, onStatusListener: OnStatusListener?) {
        this.onStatusListener = onStatusListener
        this.onStatusListener?.onProgress()
        d("infoinfo", "message ${message.message}")
        if (message.message == "") {
            d("infoinfo", "empty")
            this.onStatusListener?.onFailed("message is empty")
            return
        }
        ref.child("admin/contactUs/").push().setValue(message).addOnCompleteListener { it ->
            when {
                it.isSuccessful || it.isComplete -> {
                    this.onStatusListener?.onSuccess()
                }
                it.isCanceled -> {
                    this.onStatusListener?.onFailed("canceled")
                }
                else -> {
                    it.addOnFailureListener {
                        this.onStatusListener?.onFailed(
                            it.message.toString()
                        )
                    }
                }
            }
        }
    }

    fun getUser(onReadListener: OnReadListener) {
        val mail = SharedPreferences.getString(SharedPreferences.MAIL_FOR_FIREBASE)
        d("userinfoinfo", mail.toString())
        myRef.child("$mail").addChildEventListener(object : ChildEventListener {
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onChildMoved(p0: DataSnapshot, p1: String?) {
            }

            override fun onChildChanged(p0: DataSnapshot, p1: String?) {

            }

            override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                onReadListener.readUser(p0.getValue(User::class.java)!!)
            }

            override fun onChildRemoved(p0: DataSnapshot) {
            }

        })
    }
}

